<?php
require_once dirname(__FILE__).'/../Config/DB.php';

class mRsu
{
    public $region;
    public $eje;
    public $id_nube;
    public $nombreTema;
    public $valorTema;
    public function getNombreTema()
    {
        return $this->nombreTema;
    }
    public function setNombreTema($nombreTema)
    {
        $this->nombreTema = $nombreTema;
    }
    public function getValorTema()
    {
        return $this->valorTema;
    }
    public function setValorTema($valorTema)
    {
        $this->valorTema = $valorTema;
    }
    public function getIdNube()
    {
        return $this->id_nube;
    }
    public function setIdNube($id_nube)
    {
        $this->id_nube = $id_nube;
    }
    public function getRegion()
    {
        return $this->region;
    }
    public function setRegion($region)
    {
        $this->region = $region;
    }
    public function getEje()
    {
        return $this->eje;
    }
    public function setEje($eje)
    {
        $this->eje = $eje;
    }
    public function eje(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from rsu_c_eje  order by Nombre_eje asc";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);


        return $result;
    }

    public function regiones(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $sql = "select * from rsu_c_region  order by Nombre_Region asc";

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }




    public function unaRegion(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id_eje = $this->getEje();
        $id_region = $this->getRegion();
        $sql = "select * from rsu_c_region where id_region = '$id_region'";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }
    public function unEje(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id_eje = $this->getEje();
        $id_region = $this->getRegion();
        $sql = "select * from rsu_c_eje where id_eje = '$id_eje'";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }

    public function Menu(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id_eje = $this->getEje();
        $id_region = $this->getRegion();



        $sql = "
select * from rsu_c_buscador
    inner join rsu_c_eje rce on rsu_c_buscador.id_eje = rce.id_eje
    inner join rsu_c_region rcr on rsu_c_buscador.id_region = rcr.id_region
    inner join rsu_c_problema_general rcpg on rsu_c_buscador.id_pro_gen = rcpg.id_p_ge_rsu where rce.id_eje = '$id_eje' and rcr.id_region = '$id_region' 
";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function informacionTabla(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $id_eje = $this->getEje();
        $id_region = $this->getRegion();


        $sql = "";
        if ($id_eje == 8){
            $sql = "select rct.Nombre_tema, rcpg.problema_general, rcr.Nombre_Region,rcp.problema_p from rsu_c_buscador rscb
    inner join rsu_c_eje rce on rscb.id_eje = rce.id_eje
    inner join rsu_c_region rcr on rscb.id_region = rcr.id_region
    inner join rsu_c_problema_general rcpg on rscb.id_pro_gen = rcpg.id_p_ge_rsu
    inner join rsu_c_problema rcp on rscb.id_pro = rcp.id_problema
    inner join rsu_c_temas rct on rscb.id_t = rct.id_temas
    inner join rsu_c_mesa rcm on rscb.id_m = rcm.id_mesa
    where  rcr.id_region = '$id_region'";
        }else{

            $sql = "
select rct.Nombre_tema, rcpg.problema_general, rcr.Nombre_Region,rcp.problema_p  from rsu_c_buscador rscb
    inner join rsu_c_eje rce on rscb.id_eje = rce.id_eje
    inner join rsu_c_region rcr on rscb.id_region = rcr.id_region
    inner join rsu_c_problema_general rcpg on rscb.id_pro_gen = rcpg.id_p_ge_rsu
    inner join rsu_c_problema rcp on rscb.id_pro = rcp.id_problema
    inner join rsu_c_temas rct on rscb.id_t = rct.id_temas
    inner join rsu_c_mesa rcm on rscb.id_m = rcm.id_mesa
    where rce.id_eje = '$id_eje' and rcr.id_region = '$id_region' 
";

        }






        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);

        if ($result){
            return $result;
        }else{
            return 0;
        }

    }


    public function temasImprimir(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $id_eje = $this->getEje();
        $id_eje = $this->getEje();

        $sql = "";

        if ($id_eje == 8){
            $sql = "select * from rsu_c_temas";
        }else{
            $sql = " select * from rsu_c_temas where id_eje_fk = '$id_eje' ";
        }

        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    public function nube(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $id = $this->getIdNube();





        $sql = "
    select replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(rcp.problema_p,'Falta',''),' otros ',' '), ' las ', ' '),' carga ',''),' marco ', ' '),' uso ',' '),' otras ',' '),' ni ', ' '),' el ', ' '),')', ' '),' (',' '),'Se ', ''),' del ',' '),' o ',' '),' e ',' '),' tres ',' '),' los ',' '),' desde ',' '),' que ',' '),' sobre ', ' '),'En ',''),' por ',' '),'Uso ', ''),' al ',' '),' con ',' '),' falta ',' '),' poco ',' '),' se ',' '),' no ',' '),' a ',' '),' ha ',' '),' y ',' '),' para ',' '),' la ',' '),' en ',' '),' hay ',' '),' sin ',' '), 'No ',' '),' de ',' ') as muestra from rsu_c_buscador rscb
    inner join rsu_c_eje rce on rscb.id_eje = rce.id_eje
    inner join rsu_c_region rcr on rscb.id_region = rcr.id_region
    inner join rsu_c_problema_general rcpg on rscb.id_pro_gen = rcpg.id_p_ge_rsu
    inner join rsu_c_problema rcp on rscb.id_pro = rcp.id_problema
    inner join rsu_c_temas rct on rscb.id_t = rct.id_temas
    inner join rsu_c_mesa rcm on rscb.id_m = rcm.id_mesa
    where rct.id_temas = '$id' 
";
        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);
        return $result;

    }


    public function grafica(){
        $conexion = new DB();
        $conn = $conexion->connection();

        $id_eje = $this->getEje();
        $id_region = $this->getRegion();

        $sql = "";
        if ($id_eje == 8){
            $sql =  "    
        select SUM(rct.Nombre_tema = 'Desarrollo de avances tecnológicos y científicos en salud') as Desarrollo_de_avances_tecnológicos_y_científicos_en_salud,
           SUM(rct.Nombre_tema = 'Participación en el desarrollo de políticas y acciones de salud pública') as Participación_en_el_desarrollo_de_políticas_y_acciones_de_salud_pública,
           SUM(rct.Nombre_tema = 'Participación comunitaria para la promoción de acciones de salud integral') as Participación_comunitaria_para_la_promoción_de_acciones_de_salud_integral,
           SUM(rct.Nombre_tema = 'Docencia, investigación, extensión') as Docencia_investigación_extensión,
           SUM(rct.Nombre_tema = 'Formación integral del estudiantado/del profesorado') as Formación_integral_del_estudiantado_del_profesorado,
           SUM(rct.Nombre_tema = 'Gobernanza y gestión educativa institucional') as Gobernanza_y_gestión_educativa_institucional,
           SUM(rct.Nombre_tema = 'Transversalización de temas sociales emergentes en los procesos educativos') as Transversalización_de_temas_sociales_emergentes_en_los_procesos_educativos,
           SUM(rct.Nombre_tema = 'Discriminación y racismo') as Discriminación_y_racismo,
           SUM(rct.Nombre_tema = 'Exclusión') as Exclusión,
           SUM(rct.Nombre_tema = 'Inequidad y desigualdad') as Inequidad_y_desigualdad,
           SUM(rct.Nombre_tema = 'Subordinación') as Subordinación,
           SUM(rct.Nombre_tema = 'Desigualdades educativas') as Desigualdades_educativas,
           SUM(rct.Nombre_tema = 'Desigualdades lingüísticas') as Desigualdades_lingüísticas,
           SUM(rct.Nombre_tema = 'Movilidad humana') as Movilidad_humana,
           SUM(rct.Nombre_tema = 'Pobreza') as Pobreza,
           SUM(rct.Nombre_tema = 'Agua, energía, clima') as Agua_energía_clima,
           SUM(rct.Nombre_tema = 'Ciudades y comunidades sustentables') as Ciudades_y_comunidades_sustentables,
           SUM(rct.Nombre_tema = 'Ecosistemas terrestres y marinos') as Ecosistemas_terrestres_y_marinos,
           SUM(rct.Nombre_tema = 'Ejercicio físico y salud en espacios universitarios') as Ejercicio_físico_y_salud_en_espacios_universitarios,
           SUM(rct.Nombre_tema = 'Sistemas socioambientales') as Sistemas_socioambientales,
           SUM(rct.Nombre_tema = 'Sustentabilidad humana') as Sustentabilidad_humana,
           SUM(rct.Nombre_tema = 'Sistemas socioambientales') as Sistemas_socioambientales,
           SUM(rct.Nombre_tema = 'Empleo') as Empleo,
           SUM(rct.Nombre_tema = 'Emprendimiento') as Emprendimiento,
           SUM(rct.Nombre_tema = 'Innovación') as Innovación,
           SUM(rct.Nombre_tema = 'Interculturalidad') as Interculturalidad,
           SUM(rct.Nombre_tema = 'Instituciones sólidas') as Instituciones_sólidas,
           SUM(rct.Nombre_tema = 'Movimientos sociales') as Movimientos_sociales
    from rsu_c_buscador rscb
    inner join rsu_c_eje rce on rscb.id_eje = rce.id_eje
    inner join rsu_c_region rcr on rscb.id_region = rcr.id_region
    inner join rsu_c_problema_general rcpg on rscb.id_pro_gen = rcpg.id_p_ge_rsu
    inner join rsu_c_problema rcp on rscb.id_pro = rcp.id_problema
    inner join rsu_c_temas rct on rscb.id_t = rct.id_temas
    inner join rsu_c_mesa rcm on rscb.id_m = rcm.id_mesa
    where rcr.id_region = '$id_region';
        ";
        }else{
            $sql =  "    
        select SUM(rct.Nombre_tema = 'Desarrollo de avances tecnológicos y científicos en salud') as Desarrollo_de_avances_tecnológicos_y_científicos_en_salud,
           SUM(rct.Nombre_tema = 'Participación en el desarrollo de políticas y acciones de salud pública') as Participación_en_el_desarrollo_de_políticas_y_acciones_de_salud_pública,
           SUM(rct.Nombre_tema = 'Participación comunitaria para la promoción de acciones de salud integral') as Participación_comunitaria_para_la_promoción_de_acciones_de_salud_integral,
           SUM(rct.Nombre_tema = 'Docencia, investigación, extensión') as Docencia_investigación_extensión,
           SUM(rct.Nombre_tema = 'Formación integral del estudiantado/del profesorado') as Formación_integral_del_estudiantado_del_profesorado,
           SUM(rct.Nombre_tema = 'Gobernanza y gestión educativa institucional') as Gobernanza_y_gestión_educativa_institucional,
           SUM(rct.Nombre_tema = 'Transversalización de temas sociales emergentes en los procesos educativos') as Transversalización_de_temas_sociales_emergentes_en_los_procesos_educativos,
           SUM(rct.Nombre_tema = 'Discriminación y racismo') as Discriminación_y_racismo,
           SUM(rct.Nombre_tema = 'Exclusión') as Exclusión,
           SUM(rct.Nombre_tema = 'Inequidad y desigualdad') as Inequidad_y_desigualdad,
           SUM(rct.Nombre_tema = 'Subordinación') as Subordinación,
           SUM(rct.Nombre_tema = 'Desigualdades educativas') as Desigualdades_educativas,
           SUM(rct.Nombre_tema = 'Desigualdades lingüísticas') as Desigualdades_lingüísticas,
           SUM(rct.Nombre_tema = 'Movilidad humana') as Movilidad_humana,
           SUM(rct.Nombre_tema = 'Pobreza') as Pobreza,
           SUM(rct.Nombre_tema = 'Agua, energía, clima') as Agua_energía_clima,
           SUM(rct.Nombre_tema = 'Ciudades y comunidades sustentables') as Ciudades_y_comunidades_sustentables,
           SUM(rct.Nombre_tema = 'Ecosistemas terrestres y marinos') as Ecosistemas_terrestres_y_marinos,
           SUM(rct.Nombre_tema = 'Ejercicio físico y salud en espacios universitarios') as Ejercicio_físico_y_salud_en_espacios_universitarios,
           SUM(rct.Nombre_tema = 'Sistemas socioambientales') as Sistemas_socioambientales,
           SUM(rct.Nombre_tema = 'Sustentabilidad humana') as Sustentabilidad_humana,
           SUM(rct.Nombre_tema = 'Sistemas socioambientales') as Sistemas_socioambientales,
           SUM(rct.Nombre_tema = 'Empleo') as Empleo,
           SUM(rct.Nombre_tema = 'Emprendimiento') as Emprendimiento,
           SUM(rct.Nombre_tema = 'Innovación') as Innovación,
           SUM(rct.Nombre_tema = 'Interculturalidad') as Interculturalidad,
           SUM(rct.Nombre_tema = 'Instituciones sólidas') as Instituciones_sólidas,
           SUM(rct.Nombre_tema = 'Movimientos sociales') as Movimientos_sociales
    from rsu_c_buscador rscb
    inner join rsu_c_eje rce on rscb.id_eje = rce.id_eje
    inner join rsu_c_region rcr on rscb.id_region = rcr.id_region
    inner join rsu_c_problema_general rcpg on rscb.id_pro_gen = rcpg.id_p_ge_rsu
    inner join rsu_c_problema rcp on rscb.id_pro = rcp.id_problema
    inner join rsu_c_temas rct on rscb.id_t = rct.id_temas
    inner join rsu_c_mesa rcm on rscb.id_m = rcm.id_mesa
    where rce.id_eje = '$id_eje' and rcr.id_region = '$id_region'
        ";
        }



        $puntos = $conn->prepare($sql);
        $puntos->execute();
        $result = $puntos->fetchAll(PDO::FETCH_OBJ);



        return $result;
    }

    public function dataJsonGrafica(){

       $data = mRsu::grafica();

        $muestra = array();

        if($data[0]->Desarrollo_de_avances_tecnológicos_y_científicos_en_salud > 0){
            $data[]=array(
                "country" => "Desarrollo de avances tecnológicos y científicos en salud'",
                "visits" => $data[0]->Desarrollo_de_avances_tecnológicos_y_científicos_en_salud
            );

        }

        if($data[0]->Participación_en_el_desarrollo_de_políticas_y_acciones_de_salud_pública > 0){
            $data[]=array(
                "country" => "Participación en el desarrollo de políticas y acciones de salud pública'",
                "visits" => $data[0]->Participación_en_el_desarrollo_de_políticas_y_acciones_de_salud_pública
            );

        }

        if($data[0]->Participación_comunitaria_para_la_promoción_de_acciones_de_salud_integral > 0){
            $data[]=array(
                "country" => "Participación comunitaria para la promoción de acciones de salud integral",
                "visits" => $data[0]->Participación_comunitaria_para_la_promoción_de_acciones_de_salud_integral
            );

        }

        if($data[0]->Docencia_investigación_extensión > 0){
            $data[]=array(
                "country" => "Docencia investigación extensión",
                "visits" => $data[0]->Docencia_investigación_extensión
            );

        }

        if($data[0]-> Gobernanza_y_gestión_educativa_institucional> 0){
            $data[]=array(
                "country" => "Gobernanza y gestión educativa institucional",
                "visits" =>$data[0]->Gobernanza_y_gestión_educativa_institucional
            );

        }
        if($data[0]->Transversalización_de_temas_sociales_emergentes_en_los_procesos_educativos > 0){
            $data[]=array(
                "country" => "Transversalización de temas sociales emergentes en los procesos educativos",
                "visits" =>$data[0]->Transversalización_de_temas_sociales_emergentes_en_los_procesos_educativos
            );

        }
        if($data[0]->Discriminación_y_racismo > 0){
            $data[]=array(
                "country" => "Discriminación y racismo",
                "visits" =>$data[0]->Discriminación_y_racismo
            );

        }

        if($data[0]->Exclusión > 0){
            $data[]=array(
                "country" => "Exclusión",
                "visits" => $data[0]->Exclusión
            );

        }

        if($data[0]->Inequidad_y_desigualdad > 0){
            $data[]=array(
                "country" => "Inequidad y desigualdad",
                "visits" =>$data[0]->Inequidad_y_desigualdad
            );
        }

        if($data[0]->Subordinación > 0){
            $data[]=array(
                "country" => "Subordinación",
                "visits" => $data[0]->Subordinación
            );
        }

        if($data[0]->Desigualdades_educativas > 0){
            $data[]=array(
                "country" => "Desigualdades educativas",
                "visits" =>$data[0]->Desigualdades_educativas
            );
        }

        if($data[0]->Desigualdades_lingüísticas > 0){
            $data[]=array(
                "country" => "Desigualdades lingüísticas",
                "visits" =>$data[0]->Desigualdades_lingüísticas
            );

        }

        if($data[0]->Movilidad_humana > 0){
            $data[]=array(
                "country" => "Movilidad humana",
                "visits" =>$data[0]->Movilidad_humana
            );
        }

        if($data[0]->Pobreza > 0){
            $data[]=array(
                "country" => "Pobreza",
                "visits" =>$data[0]->Pobreza
            );
        }

        if($data[0]-> Agua_energía_clima > 0){
            $data[]=array(
                "country" => "Agua energía clima",
                "visits" =>$data[0]->Agua_energía_clima
            );
        }

        if($data[0]->Ciudades_y_comunidades_sustentables > 0){
            $data[]=array(
                "country" => "Ciudades y comunidades sustentables",
                "visits" =>$data[0]->Ciudades_y_comunidades_sustentables
            );
        }

        if($data[0]->Ecosistemas_terrestres_y_marinos> 0){
            $data[]=array(
                "country" => "Ecosistemas terrestres y marinos",
                "visits" =>$data[0]->Ecosistemas_terrestres_y_marinos
            );

        }

        if($data[0]->Ejercicio_físico_y_salud_en_espacios_universitarios> 0){
            $data[]=array(
                "country" => "Ejercicio físico y salud en espacios universitarios",
                "visits" =>$data[0]->Ejercicio_físico_y_salud_en_espacios_universitarios
            );

        }
        if($data[0]->Sistemas_socioambientales > 0){
            $data[]=array(
                "country" => "Sistemas socioambientales",
                "visits" => $data[0]->Sistemas_socioambientales
            );

        }
        if($data[0]->Sustentabilidad_humana> 0) {
            $data[]=array(
                "country" => "Sustentabilidad humana",
                "visits" =>$data[0]->Sustentabilidad_humana
            );

        }
        if($data[0]->Empleo> 0){
            $data[]=array(
                "country" => "Empleo",
                "visits" =>$data[0]->Empleo
            );

        }
        if($data[0]->Emprendimiento > 0){
            $data[]=array(
                "country" => "Emprendimiento",
                "visits" =>$data[0]->Emprendimiento
            );

        }
        if($data[0]->Innovación> 0){
            $data[]=array(
                "country" => "Innovación",
                "visits" =>$data[0]->Innovación
            );

        }
        if($data[0]->Interculturalidad > 0){
            $data[]=array(
                "country" => "Interculturalidad",
                "visits" =>$data[0]->Interculturalidad
            );

        }

        if($data[0]->Instituciones_sólidas> 0){
            $data[]=array(
                "country" => "Instituciones sólidas",
                "visits" => $data[0]->Instituciones_sólidas
            );

        }

        if($data[0]->Movimientos_sociales> 0){
            $data[]=array(
                "country" => "Movimientos sociales",
                "visits" => $data[0]->Movimientos_sociales
            );

        }
        return json_encode($data,true);
    }


    function cbase(){
        $conexion = new DB();
        $conn = $conexion->connection();

        return $conn;
    }










}