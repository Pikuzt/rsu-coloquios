/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 5.7.24 : Database - cuo-mRsu-coloquios
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cuo-rsu-coloquios` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cuo-rsu-coloquios`;

/*Table structure for table `asistentes` */

DROP TABLE IF EXISTS `asistentes`;

CREATE TABLE `asistentes` (
  `id_asistente` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Asistente` varchar(150) NOT NULL,
  `Grado_Academico` varchar(150) NOT NULL,
  `Cargo` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `Area_Academica` varchar(150) NOT NULL,
  PRIMARY KEY (`id_asistente`),
  KEY `id_region` (`id_region`),
  CONSTRAINT `id_region` FOREIGN KEY (`id_region`) REFERENCES `region` (`id_region`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `asistentes` */

/*Table structure for table `eje` */

DROP TABLE IF EXISTS `eje`;

CREATE TABLE `eje` (
  `id_eje` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_eje` varchar(150) NOT NULL,
  PRIMARY KEY (`id_eje`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32;

/*Data for the table `eje` */

insert  into `eje`(`id_eje`,`Nombre_eje`) values 
(1,'Salud'),
(2,'Educación'),
(3,'Violencias estructurales'),
(4,'Desigualdades sociales'),
(5,'Sustentabilidad'),
(6,'Mercado, producción, consumo e innovación tecnológica'),
(7,'Construcción democrática');

/*Table structure for table `estrategiaoportunidades` */

DROP TABLE IF EXISTS `estrategiaoportunidades`;

CREATE TABLE `estrategiaoportunidades` (
  `id_estrategia` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `Nombre_Oportunidad` varchar(150) NOT NULL,
  PRIMARY KEY (`id_estrategia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `estrategiaoportunidades` */

/*Table structure for table `mesa` */

DROP TABLE IF EXISTS `mesa`;

CREATE TABLE `mesa` (
  `id_mesa` int(10) NOT NULL AUTO_INCREMENT,
  `NombreMesa` varchar(150) NOT NULL,
  PRIMARY KEY (`id_mesa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

/*Data for the table `mesa` */

insert  into `mesa`(`id_mesa`,`NombreMesa`) values 
(1,'1'),
(2,'2'),
(3,'3');

/*Table structure for table `mesa_por_region` */

DROP TABLE IF EXISTS `mesa_por_region`;

CREATE TABLE `mesa_por_region` (
  `id_m_r` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa_fk` int(11) DEFAULT NULL,
  `id_region_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_m_r`),
  KEY `id_mesa_fk` (`id_mesa_fk`),
  KEY `id_region_fk` (`id_region_fk`),
  CONSTRAINT `mesa_por_region_ibfk_1` FOREIGN KEY (`id_mesa_fk`) REFERENCES `mesa` (`id_mesa`),
  CONSTRAINT `mesa_por_region_ibfk_2` FOREIGN KEY (`id_region_fk`) REFERENCES `region` (`id_region`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mesa_por_region` */

/*Table structure for table `palabraclaversu` */

DROP TABLE IF EXISTS `palabraclaversu`;

CREATE TABLE `palabraclaversu` (
  `id_palabraclave` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre-PalabraClave` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_oportunidad` int(11) NOT NULL,
  `id_prioritaria` int(11) NOT NULL,
  `id_identificada` int(11) NOT NULL,
  PRIMARY KEY (`id_palabraclave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `palabraclaversu` */

/*Table structure for table `problematicaidentificadas` */

DROP TABLE IF EXISTS `problematicaidentificadas`;

CREATE TABLE `problematicaidentificadas` (
  `id_identificada` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Problematica` varchar(250) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  PRIMARY KEY (`id_identificada`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `problematicaidentificadas` */

/*Table structure for table `problematicaprioritaria` */

DROP TABLE IF EXISTS `problematicaprioritaria`;

CREATE TABLE `problematicaprioritaria` (
  `id_prioritaria` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_ProblePrincipal` varchar(150) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  PRIMARY KEY (`id_prioritaria`),
  KEY `id_tema` (`id_tema`),
  CONSTRAINT `id_tema` FOREIGN KEY (`id_tema`) REFERENCES `temas` (`id_temas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `problematicaprioritaria` */

/*Table structure for table `region` */

DROP TABLE IF EXISTS `region`;

CREATE TABLE `region` (
  `id_region` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Region` varchar(150) NOT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf32;

/*Data for the table `region` */

insert  into `region`(`id_region`,`Nombre_Region`) values 
(1,'Xalapa '),
(2,'Orizaba'),
(3,'Poza Rica'),
(4,'Veracruz ');

/*Table structure for table `temas` */

DROP TABLE IF EXISTS `temas`;

CREATE TABLE `temas` (
  `id_temas` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_tema` varchar(150) DEFAULT NULL,
  `id_eje` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_temas`),
  KEY `id_eje` (`id_eje`),
  CONSTRAINT `id_eje` FOREIGN KEY (`id_eje`) REFERENCES `eje` (`id_eje`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf32;

/*Data for the table `temas` */

insert  into `temas`(`id_temas`,`Nombre_tema`,`id_eje`) values 
(1,'Promoción de la salud integral y entornos saludables',1),
(2,'Participación en el desarrollo de políticas y acciones de salud pública',1),
(3,'Desarrollo de avances tecnológicos y científicos en salud',1),
(4,'Participación comunitaria para la promoción de acciones de salud integral',1),
(5,'Formación integral del estudiantado/del profesorado',2),
(6,'Gobernanza y gestión educativa institucional',2),
(7,'Docencia, investigación, extensión',2),
(8,'Transversalización de temas sociales',2),
(9,'emergentes en los procesos educativos',2),
(10,'Pertinencia e impacto social',2),
(11,'Inequidad y desigualdad',3),
(12,'Discriminación y racismo',3),
(13,'Exclusión',3),
(14,'Homogeneidad',3),
(15,'Subordinación',3),
(16,'Pobreza',4),
(17,'Movilidad humana',4),
(18,'Hambre/Soberanía alimentaria',4),
(19,'Desigualdades educativas',4),
(20,'Desigualdades lingüísticas',4),
(21,'Agua, energía, clima',5),
(22,'Sustentabilidad humana',5),
(23,'Sistemas socioambientales',5),
(24,'Ciudades y comunidades sustentables',5),
(25,'Ecosistemas terrestres y marinos',5),
(26,'Industria e infraestructura',6),
(27,'Innovación',6),
(28,'Empleo',6),
(29,'Emprendimiento',6),
(30,'Tecnología',6),
(31,'Paz y Justicia',7),
(32,'Instituciones sólidas',7),
(33,'Interculturalidad',7),
(34,'Memoria, identidad y patrimonio artístico y cultural',7),
(35,'Movimientos sociales',7);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
