/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 5.7.24 : Database - cuo-rsu-coloquios
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cuo-rsu-coloquios` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cuo-rsu-coloquios`;

/*Table structure for table `asistentes` */

DROP TABLE IF EXISTS `asistentes`;

CREATE TABLE `asistentes` (
  `id_asistente` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Asistente` varchar(150) NOT NULL,
  `Grado_Academico` varchar(150) NOT NULL,
  `Cargo` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `Area_Academica` varchar(150) NOT NULL,
  PRIMARY KEY (`id_asistente`),
  KEY `id_region` (`id_region`),
  CONSTRAINT `id_region` FOREIGN KEY (`id_region`) REFERENCES `rsu_c_region` (`id_region`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `asistentes` */

/*Table structure for table `eje` */

DROP TABLE IF EXISTS `eje`;

CREATE TABLE `eje` (
  `id_eje` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_eje` varchar(150) NOT NULL,
  PRIMARY KEY (`id_eje`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32;

/*Data for the table `eje` */

insert  into `eje`(`id_eje`,`Nombre_eje`) values 
(1,'Salud'),
(2,'Educación'),
(3,'Violencias estructurales'),
(4,'Desigualdades sociales'),
(5,'Sustentabilidad'),
(6,'Mercado, producción, consumo e innovación tecnológica'),
(7,'Construcción democrática');

/*Table structure for table `estrategiaoportunidades` */

DROP TABLE IF EXISTS `estrategiaoportunidades`;

CREATE TABLE `estrategiaoportunidades` (
  `id_estrategia` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `Nombre_Oportunidad` varchar(150) NOT NULL,
  PRIMARY KEY (`id_estrategia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `estrategiaoportunidades` */

/*Table structure for table `mesa` */

DROP TABLE IF EXISTS `mesa`;

CREATE TABLE `mesa` (
  `id_mesa` int(10) NOT NULL AUTO_INCREMENT,
  `NombreMesa` varchar(150) NOT NULL,
  PRIMARY KEY (`id_mesa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

/*Data for the table `mesa` */

insert  into `mesa`(`id_mesa`,`NombreMesa`) values 
(1,'1'),
(2,'2'),
(3,'3');

/*Table structure for table `mesa_por_region` */

DROP TABLE IF EXISTS `mesa_por_region`;

CREATE TABLE `mesa_por_region` (
  `id_m_r` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa_fk` int(11) DEFAULT NULL,
  `id_region_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_m_r`),
  KEY `id_mesa_fk` (`id_mesa_fk`),
  KEY `id_region_fk` (`id_region_fk`),
  CONSTRAINT `mesa_por_region_ibfk_1` FOREIGN KEY (`id_mesa_fk`) REFERENCES `rsu_c_mesa` (`id_mesa`),
  CONSTRAINT `mesa_por_region_ibfk_2` FOREIGN KEY (`id_region_fk`) REFERENCES `rsu_c_region` (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mesa_por_region` */

insert  into `mesa_por_region`(`id_m_r`,`id_mesa_fk`,`id_region_fk`) values 
(1,1,1);

/*Table structure for table `palabraclaversu` */

DROP TABLE IF EXISTS `palabraclaversu`;

CREATE TABLE `palabraclaversu` (
  `id_palabraclave` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre-PalabraClave` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_oportunidad` int(11) NOT NULL,
  `id_prioritaria` int(11) NOT NULL,
  `id_identificada` int(11) NOT NULL,
  PRIMARY KEY (`id_palabraclave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `palabraclaversu` */

/*Table structure for table `problematicaidentificadas` */

DROP TABLE IF EXISTS `problematicaidentificadas`;

CREATE TABLE `problematicaidentificadas` (
  `id_identificada` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Problematica` varchar(250) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  PRIMARY KEY (`id_identificada`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf32;

/*Data for the table `problematicaidentificadas` */

insert  into `problematicaidentificadas`(`id_identificada`,`Nombre_Problematica`,`id_region`,`id_eje`,`id_tema`,`id_mesa`) values 
(1,'Obesidad',1,1,1,1),
(2,'Obesidad',4,1,1,1),
(3,'Obesidad',2,1,1,1),
(4,'Obesidad',5,1,1,1),
(5,'Diabetes',1,1,2,1),
(6,'Diabetes',4,1,2,1),
(7,'Diabetes',3,1,2,1),
(8,'Enfermedades gastrointestinales',1,1,2,1),
(9,'Enfermedades gastrointestinales',1,1,2,1),
(10,'Lactancia materna (practicas inadecuadas)',1,1,2,1),
(11,'Enfermedades de trnasmisión sexual',1,1,2,1),
(12,'Enfermedades de trnasmisión sexual',2,1,2,1),
(13,'Adicciones',4,1,2,1),
(14,'Hipertensión',4,1,2,1),
(15,'Hipertensión',1,1,2,1),
(16,'Cancer',4,1,2,1),
(17,'Ansiedad y depresión',1,1,2,1),
(18,'Estrés',1,1,2,1),
(19,'Estrés',4,1,2,1),
(20,'Estrés',5,1,2,1),
(21,'Desnutrición',4,1,2,1),
(22,'Salud Mental',1,1,2,1);

/*Table structure for table `problematicaprioritaria` */

DROP TABLE IF EXISTS `problematicaprioritaria`;

CREATE TABLE `problematicaprioritaria` (
  `id_prioritaria` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_ProblePrincipal` varchar(150) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  PRIMARY KEY (`id_prioritaria`),
  KEY `id_tema` (`id_tema`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `problematicaprioritaria` */

/*Table structure for table `region` */

DROP TABLE IF EXISTS `region`;

CREATE TABLE `region` (
  `id_region` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Region` varchar(150) NOT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf32;

/*Data for the table `region` */

insert  into `region`(`id_region`,`Nombre_Region`) values 
(1,'Xalapa '),
(2,'Orizaba'),
(3,'Poza Rica'),
(4,'Veracruz '),
(5,'Coatzacoalcos');

/*Table structure for table `rsu_c_buscador` */

DROP TABLE IF EXISTS `rsu_c_buscador`;

CREATE TABLE `rsu_c_buscador` (
  `id_rsu_buscador` int(11) NOT NULL AUTO_INCREMENT,
  `id_eje` int(11) DEFAULT NULL,
  `id_region` int(11) DEFAULT NULL,
  `id_pro_gen` int(11) DEFAULT NULL,
  `id_pro` int(11) DEFAULT NULL,
  `id_t` int(11) DEFAULT NULL,
  `id_m` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rsu_buscador`),
  KEY `id_eje` (`id_eje`),
  KEY `id_region` (`id_region`),
  KEY `id_pro_gen` (`id_pro_gen`),
  KEY `id_problema` (`id_pro`),
  KEY `id_t` (`id_t`),
  KEY `id_m` (`id_m`),
  CONSTRAINT `rsu_c_buscador_ibfk_1` FOREIGN KEY (`id_eje`) REFERENCES `rsu_c_eje` (`id_eje`),
  CONSTRAINT `rsu_c_buscador_ibfk_2` FOREIGN KEY (`id_region`) REFERENCES `rsu_c_region` (`id_region`),
  CONSTRAINT `rsu_c_buscador_ibfk_3` FOREIGN KEY (`id_pro_gen`) REFERENCES `rsu_c_problema_general` (`id_p_ge_rsu`),
  CONSTRAINT `rsu_c_buscador_ibfk_4` FOREIGN KEY (`id_pro`) REFERENCES `rsu_c_problema` (`id_problema`),
  CONSTRAINT `rsu_c_buscador_ibfk_5` FOREIGN KEY (`id_t`) REFERENCES `rsu_c_temas` (`id_temas`),
  CONSTRAINT `rsu_c_buscador_ibfk_6` FOREIGN KEY (`id_m`) REFERENCES `rsu_c_mesa` (`id_mesa`)
) ENGINE=InnoDB AUTO_INCREMENT=345 DEFAULT CHARSET=latin1;

/*Data for the table `rsu_c_buscador` */

insert  into `rsu_c_buscador`(`id_rsu_buscador`,`id_eje`,`id_region`,`id_pro_gen`,`id_pro`,`id_t`,`id_m`) values 
(1,1,2,6,5,25,1),
(2,1,3,9,6,5,2),
(3,1,2,4,7,2,2),
(4,1,1,11,1,9,1),
(5,1,4,11,1,9,1),
(6,1,2,11,1,9,1),
(7,1,5,11,1,9,1),
(8,1,1,11,2,9,1),
(9,1,4,11,2,9,1),
(10,1,1,11,3,9,1),
(11,1,1,11,4,9,1),
(12,1,1,11,5,9,1),
(13,1,2,11,5,9,1),
(14,1,4,11,6,9,1),
(15,1,4,11,7,9,1),
(16,1,1,11,7,9,1),
(17,1,4,11,8,9,1),
(18,1,1,11,9,9,1),
(19,1,1,11,10,9,1),
(20,1,4,11,10,9,1),
(21,1,5,11,10,9,1),
(22,1,4,11,11,9,1),
(23,1,1,11,12,9,1),
(24,1,1,11,13,9,1),
(25,1,1,5,14,9,1),
(26,1,1,5,19,24,1),
(27,1,5,5,15,24,1),
(28,1,4,5,16,24,1),
(29,1,1,5,18,24,1),
(30,1,4,5,18,24,1),
(31,1,5,5,18,24,1),
(32,1,5,5,20,24,1),
(33,1,1,5,20,24,1),
(34,1,1,5,21,24,1),
(35,1,4,5,21,24,1),
(36,1,1,5,22,24,1),
(37,1,2,5,22,24,1),
(38,1,3,5,22,24,1),
(39,1,3,5,23,24,1),
(40,1,1,35,25,23,1),
(41,1,4,35,25,23,1),
(42,1,1,35,26,23,1),
(43,1,1,35,28,23,1),
(44,1,1,35,29,23,1),
(45,1,4,35,29,23,1),
(46,1,2,12,33,24,1),
(47,1,2,12,34,24,1),
(48,1,1,12,35,24,1),
(49,1,3,12,36,24,1),
(50,1,1,12,37,24,1),
(51,1,2,12,37,24,1),
(52,1,5,12,37,24,1),
(53,1,5,12,38,24,1),
(54,1,1,12,38,24,1),
(55,1,2,12,39,24,1),
(56,1,2,12,40,24,1),
(57,1,2,12,41,24,1),
(58,1,2,12,42,24,1),
(59,1,2,12,45,24,1),
(60,1,2,12,46,24,1),
(61,1,2,12,47,24,1),
(62,1,1,12,47,24,1),
(63,1,1,11,1,9,1),
(64,1,1,11,2,23,1),
(65,1,1,11,3,23,4),
(66,1,1,11,4,23,1),
(67,1,1,11,5,23,4),
(68,1,1,11,7,23,4),
(69,1,1,11,8,23,4),
(70,1,1,11,9,23,1),
(71,1,1,11,10,23,1),
(72,1,1,11,12,23,1),
(73,1,1,11,13,23,1),
(74,1,1,5,14,23,1),
(75,1,1,5,19,24,1),
(76,1,1,5,18,24,4),
(77,1,1,5,20,24,4),
(78,1,1,5,21,24,4),
(79,1,1,5,22,24,1),
(80,1,1,48,25,23,1),
(81,1,1,48,26,23,2),
(82,1,1,48,28,23,4),
(83,1,1,48,29,23,4),
(84,1,1,12,35,24,1),
(85,1,1,12,37,24,1),
(86,1,1,12,38,24,4),
(87,1,1,12,47,24,1),
(88,2,1,36,57,29,4),
(89,2,1,36,58,25,2),
(90,2,1,29,60,14,4),
(91,2,1,29,63,14,4),
(92,2,1,50,65,29,1),
(93,2,1,50,329,29,2),
(94,2,1,1,75,7,1),
(95,2,1,1,76,7,1),
(96,2,1,1,81,14,1),
(97,3,1,13,103,6,4),
(98,3,1,13,107,6,4),
(99,3,1,55,118,6,4),
(100,3,1,55,340,6,4),
(101,3,1,55,342,6,4),
(102,3,1,39,348,30,4),
(103,5,1,60,410,1,2),
(104,5,1,60,414,31,2),
(105,5,1,60,421,31,2),
(106,5,1,44,424,32,1),
(107,5,1,64,441,31,1),
(108,5,1,31,448,8,2),
(109,6,1,65,462,13,4),
(110,6,1,65,463,13,4),
(111,6,1,65,467,13,4),
(112,6,1,23,474,17,4),
(113,6,1,23,480,20,4),
(114,6,1,23,483,34,4),
(115,6,1,23,484,17,4),
(116,6,1,23,485,34,4),
(117,6,1,23,486,34,4),
(118,6,1,23,487,20,4),
(119,6,1,4,497,20,4),
(120,7,1,67,503,22,4),
(121,7,1,67,518,22,4),
(122,7,1,67,519,22,4),
(123,7,1,67,527,22,4),
(124,7,1,68,541,27,4),
(125,7,1,46,550,22,4),
(126,7,1,29,552,22,4),
(127,1,1,12,565,24,2),
(128,1,1,48,568,3,1),
(129,1,1,48,571,3,1),
(130,2,1,49,575,10,2),
(131,2,1,49,576,10,1),
(132,2,1,6,577,7,1),
(133,2,1,53,578,25,1),
(134,2,1,6,579,7,2),
(135,2,1,6,580,29,2),
(136,2,1,6,581,25,2),
(137,2,1,49,582,10,2),
(138,2,1,49,583,10,3),
(139,2,1,53,584,14,1),
(140,2,1,53,585,14,3),
(141,5,1,57,590,1,1),
(142,5,1,44,591,32,2),
(143,6,1,32,454,20,4),
(144,7,1,67,598,36,4),
(145,7,1,33,32,35,4),
(146,3,1,55,611,6,4),
(147,1,4,11,1,9,1),
(148,1,4,11,2,23,1),
(149,1,4,11,6,23,4),
(150,1,4,11,7,23,4),
(151,1,4,11,8,23,4),
(152,1,4,11,10,23,1),
(153,1,4,11,11,23,4),
(154,1,4,5,14,23,1),
(155,1,4,5,16,24,4),
(156,1,4,5,18,24,4),
(157,1,4,5,21,24,4),
(158,1,4,48,25,23,1),
(159,1,4,48,29,23,4),
(160,2,4,49,51,10,4),
(161,2,4,49,53,10,4),
(162,2,4,29,62,14,4),
(163,2,4,29,63,14,4),
(164,2,4,29,328,14,4),
(165,2,4,50,69,29,4),
(166,2,4,50,70,29,4),
(167,2,4,1,79,14,4),
(168,2,4,1,80,14,4),
(169,2,4,1,81,14,1),
(170,2,4,53,87,14,4),
(171,2,4,6,94,10,4),
(172,2,4,6,95,29,4),
(173,3,4,13,107,6,4),
(174,3,4,55,118,6,4),
(175,3,4,55,338,6,4),
(176,3,4,55,339,6,4),
(177,3,4,55,340,6,4),
(178,4,4,56,350,4,4),
(179,4,4,42,368,16,4),
(180,5,4,59,402,2,4),
(181,5,4,44,424,32,1),
(182,5,4,44,430,32,4),
(183,5,4,61,436,2,4),
(184,6,4,23,473,17,4),
(185,6,4,23,478,12,4),
(186,6,4,23,483,34,4),
(187,6,4,23,484,17,4),
(188,6,4,23,344,17,4),
(189,6,4,45,492,17,4),
(190,6,4,4,496,20,4),
(191,7,4,67,511,22,4),
(192,7,4,67,513,22,4),
(193,7,4,67,36,22,4),
(194,7,4,67,341,22,4),
(195,7,4,33,535,22,4),
(196,7,4,33,540,22,4),
(197,1,4,48,566,3,4),
(198,1,4,48,567,3,4),
(199,1,4,48,568,3,1),
(200,4,4,29,588,19,4),
(201,3,4,55,610,6,4),
(202,3,4,38,612,11,4),
(203,3,4,13,NULL,18,4),
(204,1,3,5,22,24,1),
(205,1,3,5,23,24,4),
(206,1,3,12,36,24,4),
(207,2,3,49,51,10,4),
(208,2,3,50,67,29,4),
(209,2,3,50,330,29,4),
(210,2,3,1,76,7,1),
(211,2,3,1,77,7,4),
(212,2,3,1,78,14,4),
(213,3,3,55,336,6,4),
(214,4,3,56,351,4,4),
(215,4,3,41,357,26,4),
(216,4,3,41,360,26,4),
(217,4,3,42,368,16,4),
(218,4,3,57,382,4,4),
(219,4,3,58,385,5,4),
(220,4,3,58,386,5,4),
(221,5,3,60,414,31,2),
(222,5,3,60,416,31,4),
(223,5,3,60,420,31,4),
(224,5,3,44,424,32,1),
(225,5,3,61,435,2,4),
(226,6,3,3,454,12,4),
(227,6,3,3,460,12,4),
(228,6,3,65,461,13,4),
(229,6,3,65,465,13,4),
(230,6,3,65,467,13,4),
(231,6,3,65,470,13,4),
(232,6,3,65,471,13,4),
(233,6,3,23,476,17,4),
(234,6,3,23,479,17,4),
(235,7,3,67,355,22,4),
(236,7,3,67,517,22,4),
(237,7,3,67,518,22,4),
(238,7,3,67,533,22,4),
(239,7,3,33,539,22,4),
(240,7,3,68,93,27,4),
(241,1,3,48,566,3,4),
(242,2,3,49,573,10,4),
(243,2,3,29,574,14,4),
(244,4,3,29,587,19,4),
(245,4,3,29,588,19,4),
(246,3,3,54,594,18,4),
(247,3,3,54,599,18,4),
(248,3,3,13,600,6,4),
(249,3,3,38,601,11,4),
(250,3,3,63,602,15,4),
(251,3,3,63,603,15,4),
(252,1,6,11,1,9,1),
(253,1,6,11,5,23,3),
(254,1,6,5,14,23,3),
(255,1,6,5,22,24,1),
(256,1,6,12,33,24,3),
(257,1,6,12,34,24,3),
(258,1,6,12,37,24,6),
(259,1,6,12,39,24,5),
(260,1,6,12,40,24,6),
(261,1,6,12,41,24,7),
(262,1,6,12,42,24,1),
(263,1,6,12,327,24,7),
(264,1,6,12,46,24,7),
(265,1,6,12,47,24,1),
(266,2,6,36,57,29,4),
(267,2,6,50,66,29,4),
(268,2,6,50,329,29,4),
(269,2,6,1,75,7,4),
(270,2,6,1,82,14,4),
(271,2,6,2,84,25,4),
(272,2,6,51,102,14,4),
(273,3,6,55,334,6,4),
(274,3,6,62,344,30,4),
(275,4,6,56,351,4,4),
(276,4,6,56,119,4,4),
(277,4,6,56,340,4,4),
(278,4,6,41,359,26,4),
(279,4,6,41,360,26,4),
(280,4,6,41,362,26,4),
(281,4,6,57,375,4,4),
(282,4,6,57,377,4,4),
(283,5,6,60,410,1,4),
(284,5,6,60,414,31,4),
(285,5,6,44,424,32,4),
(286,5,6,44,427,32,4),
(287,5,6,44,431,32,4),
(288,5,6,44,432,32,4),
(289,5,6,61,435,2,4),
(290,5,6,61,438,2,4),
(291,5,6,61,439,2,4),
(292,5,6,61,440,2,4),
(293,5,6,31,447,8,4),
(294,6,6,3,453,12,4),
(295,6,6,3,457,12,4),
(296,6,6,3,458,12,4),
(297,6,6,65,462,13,4),
(298,6,6,65,463,13,4),
(299,6,6,65,464,13,4),
(300,6,6,65,465,13,4),
(301,6,6,65,469,13,4),
(302,6,6,23,476,17,4),
(303,6,6,23,479,17,4),
(304,7,6,67,514,35,4),
(305,3,6,54,594,18,4),
(306,3,6,54,595,18,4),
(307,3,6,13,596,6,4),
(308,3,6,62,597,30,4),
(309,3,6,62,604,30,4),
(310,3,6,62,605,30,4),
(311,3,6,62,606,30,4),
(312,3,6,62,607,30,4),
(313,3,6,62,608,30,4),
(314,1,5,11,1,9,4),
(315,1,5,11,10,23,4),
(316,1,5,5,15,24,4),
(317,1,5,5,18,24,4),
(318,1,5,5,20,24,4),
(319,1,5,12,37,24,4),
(320,1,5,12,38,24,4),
(321,2,5,36,54,29,4),
(322,2,5,29,60,14,4),
(323,2,5,50,65,29,4),
(324,2,5,6,331,10,4),
(325,4,5,56,340,4,4),
(326,4,5,41,357,26,4),
(327,4,5,41,360,26,4),
(328,4,5,42,365,16,4),
(329,4,5,57,376,4,4),
(330,4,5,57,377,4,4),
(331,4,5,58,385,5,4),
(332,4,5,58,389,5,4),
(333,4,5,58,393,5,4),
(334,5,5,59,407,2,4),
(335,5,5,60,412,1,4),
(336,5,5,60,413,1,4),
(337,5,5,44,431,32,4),
(338,5,5,44,434,32,4),
(339,5,5,61,437,2,4),
(340,5,5,61,440,2,4),
(341,1,5,48,570,3,4),
(342,2,5,53,572,14,4),
(343,4,5,58,586,5,4),
(344,5,5,57,589,1,4);

/*Table structure for table `rsu_c_eje` */

DROP TABLE IF EXISTS `rsu_c_eje`;

CREATE TABLE `rsu_c_eje` (
  `id_eje` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_eje` varchar(150) NOT NULL,
  PRIMARY KEY (`id_eje`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_eje` */

insert  into `rsu_c_eje`(`id_eje`,`Nombre_eje`) values 
(1,'1. Salud'),
(2,'2. Educación'),
(3,'3. Violencias estructurales'),
(4,'4. Desigualdades sociales'),
(5,'5. Sustentabilidad'),
(6,'6. Mercado, producción, consumo e innovación tecnológica'),
(8,'9. Todos'),
(9,'7. Construcción democrática');

/*Table structure for table `rsu_c_mesa` */

DROP TABLE IF EXISTS `rsu_c_mesa`;

CREATE TABLE `rsu_c_mesa` (
  `id_mesa` int(10) NOT NULL AUTO_INCREMENT,
  `NombreMesa` varchar(150) NOT NULL,
  PRIMARY KEY (`id_mesa`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_mesa` */

insert  into `rsu_c_mesa`(`id_mesa`,`NombreMesa`) values 
(1,'1'),
(2,'2'),
(3,'3'),
(4,'sin mesa'),
(5,'1,2,3'),
(6,'1,3'),
(7,'1,2');

/*Table structure for table `rsu_c_palabraclaversu` */

DROP TABLE IF EXISTS `rsu_c_palabraclaversu`;

CREATE TABLE `rsu_c_palabraclaversu` (
  `id_palabraclave` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre-PalabraClave` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_oportunidad` int(11) NOT NULL,
  `id_prioritaria` int(11) NOT NULL,
  `id_identificada` int(11) NOT NULL,
  PRIMARY KEY (`id_palabraclave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_palabraclaversu` */

/*Table structure for table `rsu_c_problema` */

DROP TABLE IF EXISTS `rsu_c_problema`;

CREATE TABLE `rsu_c_problema` (
  `id_problema` int(11) NOT NULL AUTO_INCREMENT,
  `problema_p` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_problema`)
) ENGINE=InnoDB AUTO_INCREMENT=613 DEFAULT CHARSET=latin1;

/*Data for the table `rsu_c_problema` */

insert  into `rsu_c_problema`(`id_problema`,`problema_p`) values 
(1,'Obesidad'),
(2,'Diabetes'),
(3,'Enfermedades gastrointestinales'),
(4,'Lactancia materna (prácticas inadecuadas)'),
(5,'Enfermedades de transmisión sexual'),
(6,'Adicciones\r\n'),
(7,'Hipertensión'),
(8,'Cáncer'),
(9,'Ansiedad y depresión'),
(10,'Estrés'),
(11,'Desnutrición'),
(12,'Salud mental'),
(13,'VIH'),
(14,'Embarazo infantil, embarazo adolescente, embarazo no deseado'),
(15,'Automediación'),
(16,'No conciencia de la salud y mala alimentación'),
(17,'Apatía y desinterés de la población'),
(18,'Malos hábitos saludables'),
(19,'Dengue'),
(20,'No actividades físicas'),
(21,'Sedentarismo'),
(22,'No hay formación en promoción de la salud y en medicina preventiva, educación en salud'),
(23,'No se busca atención médica integral'),
(24,'Ausencia de plan de desarrollo para la comunidad'),
(25,'Políticas institucionales en salud fragmentadas, sin dirección, poco coherentes'),
(26,'Ausencia de políticas públicas'),
(27,'Desarticulación entre la academia y la gestión administrativa'),
(28,'Acciones aisladas'),
(29,'No hay planeación de carga laboral, exceso de carga laboral'),
(30,'Escasa o falta de voluntad política'),
(31,'No implementación del código de ética'),
(32,'Inseguridad'),
(33,'Marginación'),
(34,'Contaminación'),
(35,'Acoso'),
(36,'Violencia naturalizada'),
(37,'Falta de infraestructura para actividades físicas, entornos no favorables'),
(38,'No organización de horarios que permitan actividad física'),
(39,'Monodisciplina y falta de interculturalidad en salud'),
(40,'No apertura a otras disciplinas'),
(41,'Escaso diálogo con la medicina tradicional'),
(42,'Desconocimiento de la diversidad lingüística (lenguas originarias)'),
(43,'Desconocimiento de usos y costumbres de la comunidad'),
(44,'No comunicación y respeto a la diversidad lingüística'),
(45,'Falta de vinculación'),
(46,'No comunicación hacia el interior ni tampoco al exterior'),
(47,'Falta de intersectorialidad'),
(48,'Ausencia de transversalización en salud'),
(49,'No está en PTE ni el PGD 2030'),
(50,'No hay educación emocional, falta trabajo en actitudes'),
(51,'Crisis de valores: individualismo, egoísmo, enajenación, falta de autonomía'),
(52,'Se forma en la técnica no en la ciudadanía'),
(53,'Segundo idioma, enseñanza de ética'),
(54,'No se incluye en Pladea'),
(55,'Sobrecarga de trabajo para los empleados'),
(56,'Se forma para la inserción laboral, no para la mejora del medio'),
(57,'Perfil de egreso no contempla necesidades sociales'),
(58,'Desarticulación de IES y sector empresarial'),
(59,'Disparidad entre lo que se enseña en la escuela y lo que exigen centros de trabajo'),
(60,'Articulación nula entre instancias de vinculación'),
(61,'Burocratización'),
(62,'Contacto de estudiantes con la realidad se da hasta última parte de su formación'),
(63,'Relación con otros sectores débil'),
(64,'Falta de convenios colaborativos'),
(65,'Insuficiente difusión de temas transversales'),
(66,'Ausencia de objetivos del milenio'),
(67,'Falta de sensibilización a problemas ambientales, de minorías, grupos vulnerables'),
(68,'Erradicar acoso, machismo'),
(69,'Dificultad para relacionar docencia, investigación y extensión '),
(70,'Debilidad institucional en la enseñanza de la investigación'),
(71,'No se conoce lo que hay de investigación y docencia'),
(72,'Trabajo fragmentado'),
(73,'Multitud de comisiones para la realización de proyectos'),
(74,'Trabajo aislado, desarticulado'),
(75,'Ingreso, desarrollo (formación y actualización), Diversificación de carga académica'),
(76,'Falta de motivación en docentes'),
(77,'Expertos en disciplinas sin formación pedagógica'),
(78,'Crisis de valores'),
(79,'Bajos salarios'),
(80,'Sobrecarga de trabajo'),
(81,'Falta de acompañamiento psicológico y emocional'),
(82,'Falta de formación con respecto a ética'),
(83,'Evaluación vocacional'),
(84,'Cobertura insuficiente'),
(85,'Decisiones políticas para nombrar a los directivos'),
(86,'No gobierno'),
(87,'No seguimiento de convenios de colaboración en cuanto cambia administración'),
(88,'Falta de visión de autoridades'),
(89,'Resistencia al cambio'),
(90,'Pasividad'),
(91,'No conciencia de que es necesario un cambio de modelo'),
(92,'Apatía'),
(93,'Desinterés'),
(94,'No se cultiva la lectura'),
(95,'Violencia en la comunidad universitaria'),
(96,'Falta de trabajo en equipo'),
(97,'Se requiere pago de cuotas de estudiantes'),
(98,'Es complicado el transporte'),
(99,'Diseño de aulas no adecuados para evaluación integral'),
(100,'Número elevado de estudiantes por aula'),
(101,'Carencia de infraestructura'),
(102,'Dispersión geográfica limita la oportunidad de formarse integralmente'),
(103,'En diferentes ámbitos: económicos, sociales, físicos, laboral, inferiorización'),
(104,'Violencia por ser diferentes a los demás'),
(105,'Familia perpetúa modelos racistas'),
(106,'Subordinación'),
(107,'Deshumanización'),
(108,'Falta de reconocimiento a los pueblos originarios'),
(109,'Mala distribución de recursos'),
(110,'Exclusión'),
(111,'limitado acceso a la educación, el empleo, la salud, la vivienda, la alimentación, la recreación, desigualdad salarial '),
(112,'Falta de inversión'),
(113,'Creación de empleos'),
(114,'Salarios precarios'),
(115,'Inflación'),
(116,'Legislación universitaria no colabora a combatir violencia de diferentes tipos (de género, por ejemplo), contradictorias'),
(117,'Políticas públicas insuficintes'),
(118,'Falta de empatía'),
(119,'Patriarcado'),
(120,'Falta de sororidad'),
(121,'Aceptación'),
(327,'Falta de vinculación '),
(328,'Falta de convenios colaborativos '),
(329,'Acoso, machismo, problemática de género'),
(330,'Trabajo aislado, desarticulado, fragmentado'),
(331,'Apatía, desinterés, pasividad'),
(332,'Políticas públicas insuficientes'),
(333,'Normalización'),
(334,'Legitimación  de la violencia'),
(335,'Miedo'),
(336,'Indiferencia ante problemas sociales'),
(337,'Inequidad'),
(338,'Violencia sexual'),
(339,'Violencia familiar'),
(340,'Abuso de poder'),
(341,'Violencia de género'),
(342,'Feminicidio'),
(343,'No conocemos derechos humanos '),
(344,'Burocracia'),
(345,'No recursos suficientes'),
(346,'No recursos suficientes para seguimiento de casos, por ejemplo de violencia de género'),
(347,'No acceso a salud integral'),
(348,'No prevención y promoción'),
(349,'Sistema viciado'),
(350,'Delincuencia'),
(351,'Falta de recursos'),
(352,'Abuso de poder'),
(353,'Autoritarismo'),
(354,'Falta de ética'),
(355,'Corrupción'),
(356,'Falta de sensibilización'),
(357,'Distribución desigual del ingreso, desempleo'),
(358,'Crecimiento demográfico'),
(359,'Política pública no distributiva'),
(360,'Falta de recursos económicos'),
(361,'Niveles educativos bajos'),
(362,'Discriminación profesional'),
(363,'Falta de experiencia'),
(364,'Falta de conocimientos tecnológicos'),
(365,'Falta de producción en el campo'),
(366,'No se controla el precio de la producción'),
(367,'Importación de alimentos de menor calidad'),
(368,'Ausencia de apoyo gubernamental'),
(369,'Falta de políticas públicas para evitar la desnutrición'),
(370,'Presupuestos insuficientes para combate a la desnutrición'),
(371,'Programas no funcionales'),
(372,'Trámites burocráticos'),
(373,'Resistencia al cambio para evitar desnutrición'),
(374,'Diagnósticos simulados'),
(375,'Falta de acceso a educación, economía, cultural, ciudadanía, salud, discapacidades, alimentación'),
(376,'Deserción'),
(377,'Calidad educativa diferente en zonas marginales'),
(378,'Insuficientes vías de comunicación'),
(379,'Limitaciones de oferta educativa'),
(380,'Cupo limitado en licenciaturas'),
(381,'Falta de licenciaturas en modalidad virtual'),
(382,'No educación con capacidad para atender discapacidad'),
(383,'Pocas oportunidades de empleo'),
(384,'Acceso desigual a la tecnología'),
(385,'Rechazo a culturas originarias o indígenas y a sus lenguas'),
(386,'Falta de presupuesto para reducir la marginación de grupos indígenas'),
(387,'No educación bilingüe. Falta de profesores bilingües'),
(388,'No se actualiza bibliografía de los PE en lenguas indígenas'),
(389,'Falta de inclusión a personas con discapacidad'),
(390,'Estereotipos sobre indígenas, sobre mujeres'),
(391,'Entorno social no favorable'),
(392,'Estereotipos por diferencias físicas, lingüísticas'),
(393,'No se toma en cuenta a la mujer. Desigualdad de género.'),
(394,'Con las mujeres no hay complementariedad entre roles de familia, trabajo y escuela'),
(395,'Mecanismos no aplicables a todos'),
(396,'Corrupción '),
(397,'Falta de personal capacitado para vincularse con la comunidad'),
(398,'Falta de capacitación a comunidades sobre cómo pueden vender sus productos'),
(399,'Falta de convenios con empresas para emplear estudiantes '),
(400,'No se aplica normatividad (agua, energía, clima)'),
(401,'No se difunden normas'),
(402,'No cumplimiento de leyes y normativas'),
(403,'No cumplimiento del reglamento de la UV'),
(404,'Ausencia de directriz política'),
(405,'Falta de apoyo gubernamental'),
(406,'Desfases entre políticas públicas y las necesidades de las comunidades'),
(407,'Falta de gobernanza'),
(408,'Desorganización entre sedes al interior de la UV'),
(409,'No hay programa rector que integre gobierno y academia '),
(410,'Manejo de agua'),
(411,'Falta de financiamiento'),
(412,'Carencia y sobreexplotación del agua'),
(413,'Crecimiento inmoderado de población'),
(414,'Uso irracional de recursos'),
(415,'Desperdicios de recursos'),
(416,'Falta de aprovechamiento pluvial'),
(417,'Dificultades geográficas'),
(418,'Falta de ordenamiento territorial'),
(419,'Falta de planeación de la red potable y drenaje'),
(420,'Pérdida de biodiversidad'),
(421,'Desvalorización de recursos'),
(422,'Corrupción y omisión de autoridades'),
(423,'Intereses económicos y políticos'),
(424,'Falta de conciencia ética y compromiso social, pérdida de valores'),
(425,'Falta de responsabilidad social'),
(426,'Costumbres que atenta contra la gestión de políticas sustentables e irracionales en el uso de recursos'),
(427,'Falta de motivación para generar hábitos sustentables'),
(428,'Falta de ejemplo de profesores'),
(429,'No se involucra a los niños'),
(430,'Carencia de consciencia en el uso del agua y energía'),
(431,'Desinterés por el tema'),
(432,'Falta de voluntad política'),
(433,'Silencio total ante abusos'),
(434,'Falta de participación de docentes y directivos'),
(435,'No formación desde educación básica'),
(436,'No profesionalización para el combate a la inseguridad'),
(437,'Deficiente capacitación en separación de residuos'),
(438,'Carencia de educación ambiental'),
(439,'Falta de educación para la sostenibilidad'),
(440,'Falta de recursos para ecoalfabetizar a la comunidad UV'),
(441,'No se promueve respeto al planeta'),
(442,'No economías sustentables '),
(443,'No acceso a personas con discapacidad'),
(444,'Falta de presupuesto'),
(445,'No hay botes para separación'),
(446,'Falta de tecnología ambiental'),
(447,'No tecnología adecuadas para el tratamiento de aguas negras'),
(448,'Pérdida de biodiversidad '),
(449,'Marginación y pobreza'),
(450,'Baja educación formal'),
(451,'Poco acceso a la salud'),
(452,'Falta de oportunidades '),
(453,'Falta de vinculación con sectores productivos'),
(454,'Desarticulación de la universidad con los tres sectores (gobierno, empresa, sociedad)'),
(455,'Se prioriza trabajo documentado a la vinculación'),
(456,'Inexistencia de convenios con consultorías'),
(457,'Falta de relaciones empresariales'),
(458,'Falta de comunicación entre gobierno y UV'),
(459,'No hay transparencia de información en gobierno que permitan datos hacia la solución de problemas'),
(460,'Egresados sin experiencia '),
(461,'Falta de personal académico capacitado'),
(462,'Falta de acompañamiento a proyectos innovadores'),
(463,'Financiamiento y oportunidad de emprendimiento'),
(464,'Falta de conciencia social sobre el emprendimiento'),
(465,'Falta de motivación e involucramiento'),
(466,'Falta de diagnóstico permanente de mercado'),
(467,'Desinterés y falta de información sobre cultura de emprendimiento'),
(468,'Escasa difusión de eventos sobre emprendimiento'),
(469,'No espacios para emprendimiento'),
(470,'Conformismo de los estudiantes'),
(471,'Se educa para ser empleados'),
(472,'No valorar productos por falta de investigación de resultados del producto'),
(473,'Falta de conocimiento de la producción local'),
(474,'Falta de distribución de productos locales agropecuarios, forestales y no maderables'),
(475,'Baja recaudación'),
(476,'Falta de recursos en industria'),
(477,'Falta de creación de empleo'),
(478,'Limitada atención a productores'),
(479,'Falta de parques industriales'),
(480,'Falta de acceso a información sistematizada sobre servicios'),
(481,'Falta de carreteras'),
(482,'Seguridad'),
(483,'Falta de desarrollo e implementación tecnológica'),
(484,'Insuficientes recursos financieros'),
(485,'Dependencia tecnológica de otros países'),
(486,'Insuficiente relación entre tecnología y sustentabilidad'),
(487,'Escasa producción de patentes'),
(488,'Burocracia'),
(489,'Empresas no invierten en capacitación para la innovación'),
(490,'PE obsoletos '),
(491,'No políticas institucionales sobre emprendimiento'),
(492,'Ambigüedad del marco regulatorio (industria, innovación e infraestructura)'),
(493,'Desconocimiento de RSU'),
(494,'El docente no forma en RSU'),
(495,'Docentes se dedican a cumplir indicadores para productividad y no atienden lo necesario para el bien de la sociedad'),
(496,'Ausencia de cultura sostenible'),
(497,'Producción y distribución de alimentos que no contribuyen a la salud'),
(498,'Se requiere más convivencia interdisciplianaria '),
(499,'No calidad'),
(500,'No interculturalidad'),
(501,'No esfuerzo integral, entre familia, cultura, ciudadanía, escuela. '),
(502,'Individualismo'),
(503,'Falta de valores'),
(504,'Se premia antivalores (mentir, robar)'),
(505,'Al honesto no se le reconoce'),
(506,'No se premia el mérito'),
(507,'Consumismo'),
(508,'Mercantilismo'),
(509,'Relaciones de poder'),
(510,'Competencia'),
(511,'Desigualdad social y económica'),
(512,'Falta de responsabilidad'),
(513,'Prejuicios que afectan la comunicación'),
(514,'Impunidad'),
(515,'Corrupción'),
(516,'Compadrazgo al interior de la universidad'),
(517,'Violaciones a las leyes y las normas, incumplimiento'),
(518,'Inseguridad social'),
(519,'Desconfianza'),
(520,'Represión'),
(521,'No se denuncia'),
(522,'Falta de empleo y empleo de calidad'),
(523,'Brechas educativas'),
(524,'Pobreza'),
(525,'Salarios no dignos'),
(526,'Delincuencia'),
(527,'Falta de inclusión laboral (jóvenes)'),
(528,'Autoritarismo'),
(529,'Violencia de género'),
(530,'Lógicas verticales'),
(531,'Falta de criterio para resolver problemas sin violencia'),
(532,'discriminación de grupos vulnerables (comunidades rurales, lengua, vestimenta, diversidad sexual, religiosa, de género, falta de respeto de diferencia, prejuicios, estereotipos)'),
(533,'No reconocimiento del patrimonio biocultural '),
(534,'Incapacidad para lo que fueron creadas'),
(535,'No educación de calidad'),
(536,'No salud de calidad'),
(537,'No acceso a la justicia'),
(538,'Impunidad'),
(539,'Concentración de poder'),
(540,'Medios de comunicación poco éticos (fanatismo)'),
(541,'Desconocimiento de las organizaciones'),
(542,'Represión de movimiento sociales'),
(543,'No hay educación vial ni ambiental'),
(544,'No involucrarse en las causas'),
(545,'Desarticulación y desconocimiento de iniciativas sociales'),
(546,'No se promueve organización social ni pensamiento crítico'),
(547,'No ejercicio de derechos, se desconocen, ignorancia'),
(548,'No se reconoce poder de la universidad como un actor más de la sociedad'),
(549,'Neutralidad y temor a tomar posturas ante injusticias'),
(550,'No voluntad política ni respaldo institucional'),
(551,'Simulación'),
(552,'Se duplican acciones entre instituciones, no se conocen'),
(553,'Dificultades de comunicación entre sectores'),
(554,'Falta de organización'),
(555,'Hermetismo para trabajar con otras instituciones por enfocarnos en nuestro propio criterio, sin abrirse a otros puntos de vista'),
(556,'Burocracia administrativa'),
(557,'Alejamiento de las instituciones que promueven la cultura'),
(558,'Empresas con RS hacen lo contrario'),
(559,'Doble moral'),
(560,'Falta de congruencia'),
(561,'Falta de compromiso y conciencia social'),
(562,'No colaboración'),
(563,'Visión de lucro e individualista'),
(564,'Intereses políticos que no dan continuidad a programas que resultan positivos'),
(565,'Accidentes viales'),
(566,'Vincular la ciencia y tecnología a problemas de impacto social'),
(567,'Falta de formación sobre desarrollo tecnológicos, Vinculación (Desarrollo empresarial y sector salud), Difusión de investigación sobre resultados, Falta de orientación sobre desarrollo de proyectos'),
(568,'Falta de recursos humanos, materiales y financiamiento para la salud'),
(569,'Falta de apoyo para investigación'),
(570,'Mal uso de la tecnología'),
(571,'Nuevas competencias que deben desarrollar los estudiantes de ciencias de la salud'),
(572,'Reconocimiento RSU en todo Programa Educativo'),
(573,'Falta de capacitación, atención a alumnos con discapacidad'),
(574,'Desconocimiento del área laboral de los estudiantes'),
(575,'Seguimiento a las convocatorias académicas en la institución'),
(576,'Falta de recursos '),
(577,'Falta de investigación'),
(578,'Ausencia de indicadores de impacto social / Evaluación del impacto social de los proyectos de RSU'),
(579,'Mejorar la proyección e impacto social extrainstitucional'),
(580,'Desigualdad de oportunidades '),
(581,'Tecnología, manejos tecnológicos'),
(582,'Formación insuficiente en temas para profesores y estudiantes'),
(583,'Actualización de plan de estudios'),
(584,'Gestión compleja para proyectos internacionales'),
(585,'Reforma continua al plan de estudios'),
(586,'Brecha digital'),
(587,'Migración estudiantil y laboral'),
(588,'Desempleo'),
(589,'Promoción de energías alternativas por falta de suministro energético'),
(590,'Aplicación de normatividad, cumplimiento de leyes'),
(591,'Desvinculación de los egresados con las necesidades sociales'),
(592,'Desarticulación de la universidad con los tres sectores (gobierno, empresa, sociedad)'),
(593,'Desarticulación de la universidad con los tres sectores (gobierno, empresa, sociedad)'),
(594,'Brecha de desigualdad, brecha salarial'),
(595,'Violencia feminicida'),
(596,'Discriminación hacia las comunidades rurales'),
(597,'Poder concentrado en caciques y partidarios'),
(598,'Valores'),
(599,'Acceso a servicios públicos'),
(600,'manejo de los medios (mercadotecnia)'),
(601,'Exclusión por vulnerabilidad, discapacidad, edad'),
(602,'Ámbito laboral, desigualdad de salarios'),
(603,'Poca o nula participación en puestos clave'),
(604,'Normatividad no adecuada para la atención de diferentes tipos de violencia'),
(605,'Desconocimiento de la legalidad en materia de violencia'),
(606,'Falata de capacitación de servidores: revictimización'),
(607,'No hay cultura de denuncia por parte de las víctimas'),
(608,'Subordinación de las mujeres en el ámbito laborar por parte de un superior o compañero, acoso y hostigamiento'),
(609,'Acoso (bullyng/mobbing)'),
(610,'No aplica la normatividad'),
(611,'Marginación por nivel socioeconómico'),
(612,'Falta de acceso a información');

/*Table structure for table `rsu_c_problema_general` */

DROP TABLE IF EXISTS `rsu_c_problema_general`;

CREATE TABLE `rsu_c_problema_general` (
  `id_p_ge_rsu` int(11) NOT NULL AUTO_INCREMENT,
  `problema_general` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_p_ge_rsu`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

/*Data for the table `rsu_c_problema_general` */

insert  into `rsu_c_problema_general`(`id_p_ge_rsu`,`problema_general`) values 
(1,'Carrera académica'),
(2,'Cobertura, ingreso y deserción '),
(3,'Carencias en la vinculación'),
(4,'Ausencia de RSU'),
(5,'Deficiente cultura y educación en salud'),
(6,'Cultura universitaria '),
(7,'Crisis económica'),
(8,'Cultura de violencia'),
(9,'Deficiente sistema institucional para atención de violencia'),
(10,'Crisis social, económica, ambiental, de valores'),
(11,'Enfermedades crónico degenerativas '),
(12,'Entornos no favorables, no saludables, no congruentes'),
(13,'Discriminación, racismo, segregación social, marginación socioeconómica'),
(14,'Desconocimiento de nuestros derechos'),
(15,'Desigualdad de oportunidades'),
(16,'Educación. Falta de calidad e interculturalidad'),
(17,'Entornos no favorables, no saludables, no congruentes'),
(18,'Exclusión y discriminación'),
(19,'Estructura legislativa, política y de gobierno'),
(20,'Falta de formación'),
(21,'Falta de promoción de un enfoque sustentable'),
(22,'Falta de formación y cultura emprendedora'),
(23,'Falta de infraestructura económica, tecnológica y de innovación '),
(24,'Falta de participación ciudadana'),
(25,'Formación integral insuficiente'),
(26,'Formación integral Falta de vinculación'),
(27,'Insuficiente transversalización de temas contemporáneos'),
(28,'Falta de sistema de gobernanza y sistema instituciona'),
(29,'Falta de vinculación '),
(30,'Inadecuada gestión de recursos'),
(31,'Infraestructura inadecuada'),
(32,'Insuficiente trabajo interdisciplinario '),
(33,'Instituciones débiles '),
(34,'Falta de vinculación'),
(35,'Problemas de administración, gobernanza y estructura institucional'),
(36,'No permea RSU'),
(37,'Recursos e infraestructura insuficiente'),
(38,'Normatividad insuficiente, políticas'),
(39,'No promoción de salud integral'),
(40,'Violencia \r\n'),
(41,'Pobreza, desempleo, ingresos bajo'),
(42,'Producción alimentaria y Desnutrición'),
(43,'No ejercicio de derechos '),
(44,'Prácticas culturales, compromiso social y ética inadecuadas '),
(45,'Políticas y normatividad insuficientes'),
(46,'Lo político en la universidad '),
(47,'No permea la RS'),
(48,'Problemas de administración, gobernanza y estructura institucional '),
(49,'Formación integral insuficiente '),
(50,'Insuficiente transversalización de temas contemporáneos '),
(51,'Recursos e infraestructura insuficiente '),
(52,'Crisis económica'),
(53,'Falta de sistema de gobernanza y sistema institucional'),
(54,'Crisis económica '),
(55,'Cultura de violencia '),
(56,'Violencia '),
(57,'Desigualdad de oportunidades '),
(58,'Exclusión y discriminación '),
(59,'Estructura legislativa, política y de gobierno  '),
(60,'Inadecuada gestión de recursos '),
(61,'Falta de formación '),
(62,'Deficiente sistema institucional para atención de violencia '),
(63,'Desconocimiento de nuestros derechos '),
(64,'Falta de promoción de un enfoque sustentable '),
(65,'Falta de formación y cultura emprendedora '),
(66,'Educación. Falta de calidad e interculturalidad '),
(67,'Crisis social, económica, ambiental, de valores '),
(68,'Falta de participación ciudadana ');

/*Table structure for table `rsu_c_problematica_identificadas` */

DROP TABLE IF EXISTS `rsu_c_problematica_identificadas`;

CREATE TABLE `rsu_c_problematica_identificadas` (
  `id_identificada` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Problematica` varchar(250) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  PRIMARY KEY (`id_identificada`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_problematica_identificadas` */

/*Table structure for table `rsu_c_problematica_prioritaria` */

DROP TABLE IF EXISTS `rsu_c_problematica_prioritaria`;

CREATE TABLE `rsu_c_problematica_prioritaria` (
  `id_prioritaria` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_ProblePrincipal` varchar(150) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  PRIMARY KEY (`id_prioritaria`),
  KEY `id_tema` (`id_tema`),
  CONSTRAINT `id_tema` FOREIGN KEY (`id_tema`) REFERENCES `rsu_c_temas` (`id_temas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_problematica_prioritaria` */

/*Table structure for table `rsu_c_region` */

DROP TABLE IF EXISTS `rsu_c_region`;

CREATE TABLE `rsu_c_region` (
  `id_region` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Region` varchar(150) NOT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_region` */

insert  into `rsu_c_region`(`id_region`,`Nombre_Region`) values 
(1,'Xalapa '),
(2,'Orizaba'),
(3,'Poza Rica'),
(4,'Veracruz '),
(5,'Coatzacoalcos'),
(6,'Córdoba');

/*Table structure for table `rsu_c_temas` */

DROP TABLE IF EXISTS `rsu_c_temas`;

CREATE TABLE `rsu_c_temas` (
  `id_temas` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_tema` varchar(150) DEFAULT NULL,
  `id_eje_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_temas`),
  KEY `id_eje_fk` (`id_eje_fk`),
  CONSTRAINT `rsu_c_temas_ibfk_1` FOREIGN KEY (`id_eje_fk`) REFERENCES `rsu_c_eje` (`id_eje`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_temas` */

insert  into `rsu_c_temas`(`id_temas`,`Nombre_tema`,`id_eje_fk`) values 
(1,'Agua, energía, clima',5),
(2,'Ciudades y comunidades sustentables',5),
(3,'Desarrollo de avances tecnológicos y científicos en salud',1),
(4,'Desigualdades educativas',4),
(5,'Desigualdades lingüísticas',4),
(6,'Discriminación y racismo',3),
(7,'Docencia, investigación, extensión',2),
(8,'Ecosistemas terrestres y marinos',5),
(9,'Ejercicio físico y salud en espacios universitarios',5),
(10,'Formación integral del estudiantado/del profesorado',2),
(11,'Exclusión',3),
(12,'Empleo\r\n',6),
(13,'Emprendimiento\r\n',6),
(14,'Gobernanza y gestión educativa institucional',2),
(15,'Homogeneidad\r\n',3),
(16,'Hambre/Soberanía alimentaria\r\n',4),
(17,'Industria e infraestructura',6),
(18,'Inequidad y desigualdad\r\n',3),
(19,'Movilidad humana\r\n',4),
(20,'Innovación',6),
(21,'Interculturalidad',9),
(22,'Instituciones sólidas\r\n',9),
(23,'Participación en el desarrollo de políticas y acciones de salud pública',1),
(24,'Participación comunitaria para la promoción de acciones de salud integral',1),
(25,'Pertinencia e impacto social\r\n',2),
(26,'Pobreza\r\n',4),
(27,'Movimientos sociales\r\n',9),
(29,'Transversalización de temas sociales emergentes en los procesos educativos\r\n',2),
(30,'Subordinación\r\n',3),
(31,'Sistemas socioambientales\r\n',5),
(32,'Sustentabilidad humana',5),
(33,'Sistemas socioambientales\r\n',5);

/*Table structure for table `temas` */

DROP TABLE IF EXISTS `temas`;

CREATE TABLE `temas` (
  `id_temas` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_tema` varchar(150) DEFAULT NULL,
  `id_eje` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_temas`),
  KEY `id_eje` (`id_eje`),
  CONSTRAINT `id_eje` FOREIGN KEY (`id_eje`) REFERENCES `eje` (`id_eje`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf32;

/*Data for the table `temas` */

insert  into `temas`(`id_temas`,`Nombre_tema`,`id_eje`) values 
(1,'Promoción de la salud integral y entornos saludables',1),
(2,'Participación en el desarrollo de políticas y acciones de salud pública',1),
(3,'Desarrollo de avances tecnológicos y científicos en salud',1),
(4,'Participación comunitaria para la promoción de acciones de salud integral',1),
(5,'Formación integral del estudiantado/del profesorado',2),
(6,'Gobernanza y gestión educativa institucional',2),
(7,'Docencia, investigación, extensión',2),
(8,'Transversalización de temas sociales',2),
(9,'emergentes en los procesos educativos',2),
(10,'Pertinencia e impacto social',2),
(11,'Inequidad y desigualdad',3),
(12,'Discriminación y racismo',3),
(13,'Exclusión',3),
(14,'Homogeneidad',3),
(15,'Subordinación',3),
(16,'Pobreza',4),
(17,'Movilidad humana',4),
(18,'Hambre/Soberanía alimentaria',4),
(19,'Desigualdades educativas',4),
(20,'Desigualdades lingüísticas',4),
(21,'Agua, energía, clima',5),
(22,'Sustentabilidad humana',5),
(23,'Sistemas socioambientales',5),
(24,'Ciudades y comunidades sustentables',5),
(25,'Ecosistemas terrestres y marinos',5),
(26,'Industria e infraestructura',6),
(27,'Innovación',6),
(28,'Empleo',6),
(29,'Emprendimiento',6),
(30,'Tecnología',6),
(31,'Paz y Justicia',7),
(32,'Instituciones sólidas',7),
(33,'Interculturalidad',7),
(34,'Memoria, identidad y patrimonio artístico y cultural',7),
(35,'Movimientos sociales',7);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
