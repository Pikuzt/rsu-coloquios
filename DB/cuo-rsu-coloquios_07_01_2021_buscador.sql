-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-01-2021 a las 20:56:12
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cuo-rsu-coloquios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistentes`
--

CREATE TABLE `asistentes` (
  `id_asistente` int(11) NOT NULL,
  `Nombre_Asistente` varchar(150) NOT NULL,
  `Grado_Academico` varchar(150) NOT NULL,
  `Cargo` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `Area_Academica` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eje`
--

CREATE TABLE `eje` (
  `id_eje` int(11) NOT NULL,
  `Nombre_eje` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `eje`
--

INSERT INTO `eje` (`id_eje`, `Nombre_eje`) VALUES
(1, 'Salud'),
(2, 'Educación'),
(3, 'Violencias estructurales'),
(4, 'Desigualdades sociales'),
(5, 'Sustentabilidad'),
(6, 'Mercado, producción, consumo e innovación tecnológica'),
(7, 'Construcción democrática');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estrategiaoportunidades`
--

CREATE TABLE `estrategiaoportunidades` (
  `id_estrategia` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `Nombre_Oportunidad` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa`
--

CREATE TABLE `mesa` (
  `id_mesa` int(10) NOT NULL,
  `NombreMesa` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `mesa`
--

INSERT INTO `mesa` (`id_mesa`, `NombreMesa`) VALUES
(1, '1'),
(2, '2'),
(3, '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mesa_por_region`
--

CREATE TABLE `mesa_por_region` (
  `id_m_r` int(11) NOT NULL,
  `id_mesa_fk` int(11) DEFAULT NULL,
  `id_region_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mesa_por_region`
--

INSERT INTO `mesa_por_region` (`id_m_r`, `id_mesa_fk`, `id_region_fk`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabraclaversu`
--

CREATE TABLE `palabraclaversu` (
  `id_palabraclave` int(11) NOT NULL,
  `Nombre-PalabraClave` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_oportunidad` int(11) NOT NULL,
  `id_prioritaria` int(11) NOT NULL,
  `id_identificada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `problematicaidentificadas`
--

CREATE TABLE `problematicaidentificadas` (
  `id_identificada` int(11) NOT NULL,
  `Nombre_Problematica` varchar(250) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `problematicaidentificadas`
--

INSERT INTO `problematicaidentificadas` (`id_identificada`, `Nombre_Problematica`, `id_region`, `id_eje`, `id_tema`, `id_mesa`) VALUES
(1, 'Obesidad', 1, 1, 1, 1),
(2, 'Obesidad', 4, 1, 1, 1),
(3, 'Obesidad', 2, 1, 1, 1),
(4, 'Obesidad', 5, 1, 1, 1),
(5, 'Diabetes', 1, 1, 2, 1),
(6, 'Diabetes', 4, 1, 2, 1),
(7, 'Diabetes', 3, 1, 2, 1),
(8, 'Enfermedades gastrointestinales', 1, 1, 2, 1),
(9, 'Enfermedades gastrointestinales', 1, 1, 2, 1),
(10, 'Lactancia materna (practicas inadecuadas)', 1, 1, 2, 1),
(11, 'Enfermedades de trnasmisión sexual', 1, 1, 2, 1),
(12, 'Enfermedades de trnasmisión sexual', 2, 1, 2, 1),
(13, 'Adicciones', 4, 1, 2, 1),
(14, 'Hipertensión', 4, 1, 2, 1),
(15, 'Hipertensión', 1, 1, 2, 1),
(16, 'Cancer', 4, 1, 2, 1),
(17, 'Ansiedad y depresión', 1, 1, 2, 1),
(18, 'Estrés', 1, 1, 2, 1),
(19, 'Estrés', 4, 1, 2, 1),
(20, 'Estrés', 5, 1, 2, 1),
(21, 'Desnutrición', 4, 1, 2, 1),
(22, 'Salud Mental', 1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `problematicaprioritaria`
--

CREATE TABLE `problematicaprioritaria` (
  `id_prioritaria` int(11) NOT NULL,
  `Nombre_ProblePrincipal` varchar(150) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE `region` (
  `id_region` int(11) NOT NULL,
  `Nombre_Region` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`id_region`, `Nombre_Region`) VALUES
(1, 'Xalapa '),
(2, 'Orizaba'),
(3, 'Poza Rica'),
(4, 'Veracruz '),
(5, 'Coatzacoalcos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_buscador`
--

CREATE TABLE `rsu_c_buscador` (
  `id_rsu_buscador` int(11) NOT NULL,
  `id_eje` int(11) DEFAULT NULL,
  `id_region` int(11) DEFAULT NULL,
  `id_pro_gen` int(11) DEFAULT NULL,
  `id_pro` int(11) DEFAULT NULL,
  `id_t` int(11) DEFAULT NULL,
  `id_m` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rsu_c_buscador`
--

INSERT INTO `rsu_c_buscador` (`id_rsu_buscador`, `id_eje`, `id_region`, `id_pro_gen`, `id_pro`, `id_t`, `id_m`) VALUES
(1, 1, 3, 20, 1, 3, 1),
(2, 1, 2, 6, 5, 25, 1),
(3, 1, 3, 9, 6, 5, 2),
(4, 1, 2, 4, 7, 2, 2),
(12, 1, 1, 11, 1, 9, 1),
(13, 1, 4, 11, 1, 9, 1),
(14, 1, 2, 11, 1, 9, 1),
(15, 1, 5, 11, 1, 9, 1),
(16, 1, 1, 11, 2, 9, 1),
(17, 1, 4, 11, 2, 9, 1),
(18, 1, 1, 11, 3, 9, 1),
(19, 1, 1, 11, 4, 9, 1),
(20, 1, 1, 11, 5, 9, 1),
(21, 1, 2, 11, 5, 9, 1),
(22, 1, 4, 11, 6, 9, 1),
(23, 1, 4, 11, 7, 9, 1),
(24, 1, 1, 11, 7, 9, 1),
(25, 1, 4, 11, 8, 9, 1),
(26, 1, 1, 11, 9, 9, 1),
(27, 1, 1, 11, 10, 9, 1),
(28, 1, 4, 11, 10, 9, 1),
(29, 1, 5, 11, 10, 9, 1),
(30, 1, 4, 11, 11, 9, 1),
(31, 1, 1, 11, 12, 9, 1),
(32, 1, 1, 11, 13, 9, 1),
(33, 1, 1, 5, 14, 9, 1),
(34, 1, 1, 5, 19, 24, 1),
(35, 1, 5, 5, 15, 24, 1),
(36, 1, 4, 5, 16, 24, 1),
(38, 1, 1, 5, 18, 24, 1),
(39, 1, 4, 5, 18, 24, 1),
(40, 1, 5, 5, 18, 24, 1),
(41, 1, 5, 5, 20, 24, 1),
(42, 1, 1, 5, 20, 24, 1),
(43, 1, 1, 5, 21, 24, 1),
(44, 1, 4, 5, 21, 24, 1),
(45, 1, 1, 5, 22, 24, 1),
(46, 1, 2, 5, 22, 24, 1),
(47, 1, 3, 5, 22, 24, 1),
(48, 1, 3, 5, 23, 24, 1),
(49, 1, 1, 35, 25, 23, 1),
(50, 1, 4, 35, 25, 23, 1),
(51, 1, 1, 35, 26, 23, 1),
(52, 1, 1, 35, 28, 23, 1),
(53, 1, 1, 35, 29, 23, 1),
(54, 1, 4, 35, 29, 23, 1),
(55, 1, 2, 12, 33, 24, 1),
(56, 1, 2, 12, 34, 24, 1),
(57, 1, 1, 12, 35, 24, 1),
(58, 1, 3, 12, 36, 24, 1),
(59, 1, 1, 12, 37, 24, 1),
(60, 1, 2, 12, 37, 24, 1),
(61, 1, 5, 12, 37, 24, 1),
(62, 1, 5, 12, 38, 24, 1),
(63, 1, 1, 12, 38, 24, 1),
(64, 1, 2, 12, 39, 24, 1),
(65, 1, 2, 12, 40, 24, 1),
(66, 1, 2, 12, 41, 24, 1),
(67, 1, 2, 12, 42, 24, 1),
(68, 1, 2, 12, 45, 24, 1),
(69, 1, 2, 12, 46, 24, 1),
(70, 1, 2, 12, 47, 24, 1),
(71, 1, 1, 12, 47, 24, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_eje`
--

CREATE TABLE `rsu_c_eje` (
  `id_eje` int(11) NOT NULL,
  `Nombre_eje` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `rsu_c_eje`
--

INSERT INTO `rsu_c_eje` (`id_eje`, `Nombre_eje`) VALUES
(1, 'Salud'),
(2, 'Educación'),
(3, 'Violencias estructurales'),
(4, 'Desigualdades sociales'),
(5, 'Sustentabilidad'),
(6, 'Mercado, producción, consumo e innovación tecnológica'),
(7, 'Construcción democrática'),
(8, 'Todos'),
(9, 'Democracia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_mesa`
--

CREATE TABLE `rsu_c_mesa` (
  `id_mesa` int(10) NOT NULL,
  `NombreMesa` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `rsu_c_mesa`
--

INSERT INTO `rsu_c_mesa` (`id_mesa`, `NombreMesa`) VALUES
(1, '1'),
(2, '2'),
(3, '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_palabraclaversu`
--

CREATE TABLE `rsu_c_palabraclaversu` (
  `id_palabraclave` int(11) NOT NULL,
  `Nombre-PalabraClave` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_oportunidad` int(11) NOT NULL,
  `id_prioritaria` int(11) NOT NULL,
  `id_identificada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_problema`
--

CREATE TABLE `rsu_c_problema` (
  `id_problema` int(11) NOT NULL,
  `problema_p` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rsu_c_problema`
--

INSERT INTO `rsu_c_problema` (`id_problema`, `problema_p`) VALUES
(1, 'Obesidad'),
(2, 'Diabetes\r\n'),
(3, 'Enfermedades gastrointestinales\r\n'),
(4, 'Lactancia materna (prácticas inadecuadas)\r\n'),
(5, 'Enfermedades de transmisión sexual\r\n'),
(6, 'Adicciones\r\n'),
(7, 'Hipertensión\r\n'),
(8, 'Cáncer\r\n'),
(9, 'Ansiedad y depresión\r\n'),
(10, 'Estrés'),
(11, 'Desnutrición'),
(12, 'Salud mental\r\n'),
(13, 'VIH\r\n'),
(14, 'Embarazo infantil, embarazo adolescente, embarazo no deseado'),
(15, 'Automediación'),
(16, 'No conciencia de la salud y mala alimentación'),
(17, 'Apatía y desinterés de la población'),
(18, 'Malos hábitos saludables'),
(19, 'Dengue'),
(20, 'No actividades físicas'),
(21, 'Sedentarismo'),
(22, 'No hay formación en promoción de la salud y en medicina preventiva, educación en salud'),
(23, 'No se busca atención médica integral'),
(24, 'Ausencia de plan de desarrollo para la comunidad'),
(25, 'Políticas institucionales en salud fragmentadas, sin dirección, poco coherentes'),
(26, 'Ausencia de políticas públicas'),
(27, 'Desarticulación entre la academia y la gestión administrativa'),
(28, 'Acciones aisladas'),
(29, 'No hay planeación de carga laboral, exceso de carga laboral'),
(30, 'Escasa o falta de voluntad política'),
(31, 'No implementación del código de ética'),
(32, 'Inseguridad'),
(33, 'Marginación'),
(34, 'Contaminación'),
(35, 'Acoso'),
(36, 'Violencia naturalizada'),
(37, 'Falta de infraestructura para actividades físicas, entornos no favorables'),
(38, 'No organización de horarios que permitan actividad física'),
(39, 'Monodisciplina y falta de interculturalidad en salud'),
(40, 'No apertura a otras disciplinas'),
(41, 'Escaso diálogo con la medicina tradicional'),
(42, 'Desconocimiento de la diversidad lingüística (lenguas originarias)'),
(43, 'Desconocimiento de usos y costumbres de la comunidad'),
(44, 'No comunicación y respeto a la diversidad lingüística'),
(45, 'Falta de vinculación'),
(46, 'No comunicación hacia el interior ni tampoco al exterior'),
(47, 'Falta de intersectorialidad'),
(48, 'Ausencia de transversalización en salud'),
(49, 'No está en PTE ni el PGD 2030'),
(50, 'No hay educación emocional, falta trabajo en actitudes'),
(51, 'Crisis de valores: individualismo, egoísmo, enajenación, falta de autonomía'),
(52, 'Se forma en la técnica no en la ciudadanía'),
(53, 'Segundo idioma, enseñanza de ética'),
(54, 'No se incluye en Pladea'),
(55, 'Sobrecarga de trabajo para los empleados'),
(56, 'Se forma para la inserción laboral, no para la mejora del medio'),
(57, 'Perfil de egreso no contempla necesidades sociales'),
(58, 'Desarticulación de IES y sector empresarial'),
(59, 'Disparidad entre lo que se enseña en la escuela y lo que exigen centros de trabajo'),
(60, 'Articulación nula entre instancias de vinculación'),
(61, 'Burocratización'),
(62, 'Contacto de estudiantes con la realidad se da hasta última parte de su formación'),
(63, 'Relación con otros sectores débil'),
(64, 'Falta de convenios colaborativos'),
(65, 'Insuficiente difusión de temas transversales'),
(66, 'Ausencia de objetivos del milenio'),
(67, 'Falta de sensibilización a problemas ambientales, de minorías, grupos vulnerables'),
(68, 'Erradicar acoso, machismo'),
(69, 'Dificultad para relacionar docencia, investigación y extensión '),
(70, 'Debilidad institucional en la enseñanza de la investigación'),
(71, 'No se conoce lo que hay de investigación y docencia'),
(72, 'Trabajo fragmentado'),
(73, 'Multitud de comisiones para la realización de proyectos'),
(74, 'Trabajo aislado, desarticulado'),
(75, 'Ingreso, desarrollo (formación y actualización), Diversificación de carga académica'),
(76, 'Falta de motivación en docentes'),
(77, 'Expertos en disciplinas sin formación pedagógica'),
(78, 'Crisis de valores'),
(79, 'Bajos salarios'),
(80, 'Sobrecarga de trabajo'),
(81, 'Falta de acompañamiento psicológico y emocional'),
(82, 'Falta de formación con respecto a ética'),
(83, 'Evaluación vocacional'),
(84, 'Cobertura insuficiente'),
(85, 'Decisiones políticas para nombrar a los directivos'),
(86, 'No gobierno'),
(87, 'No seguimiento de convenios de colaboración en cuanto cambia administración'),
(88, 'Falta de visión de autoridades'),
(89, 'Resistencia al cambio'),
(90, 'Pasividad'),
(91, 'No conciencia de que es necesario un cambio de modelo'),
(92, 'Apatía'),
(93, 'Desinterés'),
(94, 'No se cultiva la lectura'),
(95, 'Violencia en la comunidad universitaria'),
(96, 'Falta de trabajo en equipo'),
(97, 'Se requiere pago de cuotas de estudiantes'),
(98, 'Es complicado el transporte'),
(99, 'Diseño de aulas no adecuados para evaluación integral'),
(100, 'Número elevado de estudiantes por aula'),
(101, 'Carencia de infraestructura'),
(102, 'Dispersión geográfica limita la oportunidad de formarse integralmente'),
(103, 'En diferentes ámbitos: económicos, sociales, físicos, laboral, inferiorización'),
(104, 'Violencia por ser diferentes a los demás'),
(105, 'Familia perpetúa modelos racistas'),
(106, 'Subordinación'),
(107, 'Deshumanización'),
(108, 'Falta de reconocimiento a los pueblos originarios'),
(109, 'Mala distribución de recursos'),
(110, 'Exclusión'),
(111, 'limitado acceso a la educación, el empleo, la salud, la vivienda, la alimentación, la recreación, desigualdad salarial '),
(112, 'Falta de inversión'),
(113, 'Creación de empleos'),
(114, 'Salarios precarios'),
(115, 'Inflación'),
(116, 'Legislación universitaria no colabora a combatir violencia de diferentes tipos (de género, por ejemplo), contradictorias'),
(117, 'Políticas públicas insuficintes'),
(118, 'Falta de empatía'),
(119, 'Patriarcado'),
(120, 'Falta de sororidad'),
(121, 'Aceptación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_problematica_identificadas`
--

CREATE TABLE `rsu_c_problematica_identificadas` (
  `id_identificada` int(11) NOT NULL,
  `Nombre_Problematica` varchar(250) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_problematica_prioritaria`
--

CREATE TABLE `rsu_c_problematica_prioritaria` (
  `id_prioritaria` int(11) NOT NULL,
  `Nombre_ProblePrincipal` varchar(150) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_problema_general`
--

CREATE TABLE `rsu_c_problema_general` (
  `id_p_ge_rsu` int(11) NOT NULL,
  `problema_general` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rsu_c_problema_general`
--

INSERT INTO `rsu_c_problema_general` (`id_p_ge_rsu`, `problema_general`) VALUES
(1, 'Carrera académica'),
(2, 'Cobertura, ingreso y deserción '),
(3, 'Carencias en la vinculación'),
(4, 'Ausencia de RSU\r\n'),
(5, 'Deficiente cultura y educación en salud'),
(6, 'Cultura universitaria '),
(7, 'Crisis económica \r\n'),
(8, 'Cultura de violencia \r\n'),
(9, 'Deficiente sistema institucional para atención de violencia \r\n'),
(10, 'Crisis social, económica, ambiental, de valores \r\n'),
(11, 'Enfermedades crónico degenerativas '),
(12, 'Entornos no favorables, no saludables, no congruentes'),
(13, 'Discriminación, racismo, segregación social, marginación socioeconómica\r\n'),
(14, 'Desconocimiento de nuestros derechos \r\n'),
(15, 'Desigualdad de oportunidades \r\n'),
(16, 'Educación. Falta de calidad e interculturalidad \r\n'),
(17, 'Entornos no favorables, no saludables, no congruentes\r\n'),
(18, 'Exclusión y discriminación \r\n'),
(19, 'Estructura legislativa, política y de gobierno  \r\n'),
(20, 'Falta de formación \r\n'),
(21, 'Falta de promoción de un enfoque sustentable \r\n'),
(22, 'Falta de formación y cultura emprendedora \r\n'),
(23, 'Falta de infraestructura económica, tecnológica y de innovación '),
(24, 'Falta de participación ciudadana \r\n'),
(25, 'Formación integral insuficiente \r\n'),
(26, 'Formación integral Falta de vinculación \r\n'),
(27, 'Insuficiente transversalización de temas contemporáneos \r\n'),
(28, 'Falta de sistema de gobernanza y sistema instituciona\r\n'),
(29, 'Falta de vinculación \r\n'),
(30, 'Inadecuada gestión de recursos \r\n'),
(31, 'Infraestructura inadecuada\r\n'),
(32, 'Insuficiente trabajo interdisciplinario \r\n'),
(33, 'Instituciones débiles \r\n'),
(34, 'Falta de vinculación \r\n'),
(35, 'Problemas de administración, gobernanza y estructura institucional \r\n'),
(36, 'No permea RSU\r\n'),
(37, 'Recursos e infraestructura insuficiente \r\n'),
(38, 'Normatividad insuficiente, políticas\r\n'),
(39, 'No promoción de salud integral\r\n'),
(40, 'Violencia \r\n'),
(41, 'Pobreza, desempleo, ingresos bajo\r\n'),
(42, 'Producción alimentaria y Desnutrición\r\n'),
(43, 'No ejercicio de derechos \r\n'),
(44, 'Prácticas culturales, compromiso social y ética inadecuadas \r\n'),
(45, 'Políticas y normatividad insuficientes\r\n'),
(46, 'Lo político en la universidad \r\n'),
(47, 'No permea la RS\r\n'),
(48, 'Problemas de administración, gobernanza y estructura institucional \r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_region`
--

CREATE TABLE `rsu_c_region` (
  `id_region` int(11) NOT NULL,
  `Nombre_Region` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `rsu_c_region`
--

INSERT INTO `rsu_c_region` (`id_region`, `Nombre_Region`) VALUES
(1, 'Xalapa '),
(2, 'Orizaba'),
(3, 'Poza Rica'),
(4, 'Veracruz '),
(5, 'Coatzacoalcos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rsu_c_temas`
--

CREATE TABLE `rsu_c_temas` (
  `id_temas` int(11) NOT NULL,
  `Nombre_tema` varchar(150) DEFAULT NULL,
  `id_eje_fk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `rsu_c_temas`
--

INSERT INTO `rsu_c_temas` (`id_temas`, `Nombre_tema`, `id_eje_fk`) VALUES
(1, 'Agua, energía, clima', 5),
(2, 'Ciudades y comunidades sustentables', 5),
(3, 'Desarrollo de avances tecnológicos y científicos en salud', 1),
(4, 'Desigualdades educativas', 4),
(5, 'Desigualdades lingüísticas', 4),
(6, 'Discriminación y racismo', 3),
(7, 'Docencia, investigación, extensión', 2),
(8, 'Ecosistemas terrestres y marinos', 5),
(9, 'Ejercicio físico y salud en espacios universitarios', 5),
(10, 'Formación integral del estudiantado/del profesorado', 2),
(11, 'Exclusión', 3),
(12, 'Empleo\r\n', 6),
(13, 'Emprendimiento\r\n', 6),
(14, 'Gobernanza y gestión educativa institucional', 2),
(15, 'Homogeneidad\r\n', 3),
(16, 'Hambre/Soberanía alimentaria\r\n', 4),
(17, 'Industria e infraestructura', 6),
(18, 'Inequidad y desigualdad\r\n', 3),
(19, 'Movilidad humana\r\n', 4),
(20, 'Innovación', 6),
(21, 'Interculturalidad', 9),
(22, 'Instituciones sólidas\r\n', 9),
(23, 'Participación en el desarrollo de políticas y acciones de salud pública', 1),
(24, 'Participación comunitaria para la promoción de acciones de salud integral', 1),
(25, 'Pertinencia e impacto social\r\n', 2),
(26, 'Pobreza\r\n', 4),
(27, 'Movimientos sociales\r\n', 9),
(28, 'Participación comunitaria para la promoción de acciones de salud integral', 1),
(29, 'Transversalización de temas sociales emergentes en los procesos educativos\r\n', 2),
(30, 'Subordinación\r\n', 3),
(31, 'Sistemas socioambientales\r\n', 5),
(32, 'Sustentabilidad humana', 5),
(33, 'Sistemas socioambientales\r\n', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temas`
--

CREATE TABLE `temas` (
  `id_temas` int(11) NOT NULL,
  `Nombre_tema` varchar(150) DEFAULT NULL,
  `id_eje` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Volcado de datos para la tabla `temas`
--

INSERT INTO `temas` (`id_temas`, `Nombre_tema`, `id_eje`) VALUES
(1, 'Promoción de la salud integral y entornos saludables', 1),
(2, 'Participación en el desarrollo de políticas y acciones de salud pública', 1),
(3, 'Desarrollo de avances tecnológicos y científicos en salud', 1),
(4, 'Participación comunitaria para la promoción de acciones de salud integral', 1),
(5, 'Formación integral del estudiantado/del profesorado', 2),
(6, 'Gobernanza y gestión educativa institucional', 2),
(7, 'Docencia, investigación, extensión', 2),
(8, 'Transversalización de temas sociales', 2),
(9, 'emergentes en los procesos educativos', 2),
(10, 'Pertinencia e impacto social', 2),
(11, 'Inequidad y desigualdad', 3),
(12, 'Discriminación y racismo', 3),
(13, 'Exclusión', 3),
(14, 'Homogeneidad', 3),
(15, 'Subordinación', 3),
(16, 'Pobreza', 4),
(17, 'Movilidad humana', 4),
(18, 'Hambre/Soberanía alimentaria', 4),
(19, 'Desigualdades educativas', 4),
(20, 'Desigualdades lingüísticas', 4),
(21, 'Agua, energía, clima', 5),
(22, 'Sustentabilidad humana', 5),
(23, 'Sistemas socioambientales', 5),
(24, 'Ciudades y comunidades sustentables', 5),
(25, 'Ecosistemas terrestres y marinos', 5),
(26, 'Industria e infraestructura', 6),
(27, 'Innovación', 6),
(28, 'Empleo', 6),
(29, 'Emprendimiento', 6),
(30, 'Tecnología', 6),
(31, 'Paz y Justicia', 7),
(32, 'Instituciones sólidas', 7),
(33, 'Interculturalidad', 7),
(34, 'Memoria, identidad y patrimonio artístico y cultural', 7),
(35, 'Movimientos sociales', 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asistentes`
--
ALTER TABLE `asistentes`
  ADD PRIMARY KEY (`id_asistente`),
  ADD KEY `id_region` (`id_region`);

--
-- Indices de la tabla `eje`
--
ALTER TABLE `eje`
  ADD PRIMARY KEY (`id_eje`);

--
-- Indices de la tabla `estrategiaoportunidades`
--
ALTER TABLE `estrategiaoportunidades`
  ADD PRIMARY KEY (`id_estrategia`);

--
-- Indices de la tabla `mesa`
--
ALTER TABLE `mesa`
  ADD PRIMARY KEY (`id_mesa`);

--
-- Indices de la tabla `mesa_por_region`
--
ALTER TABLE `mesa_por_region`
  ADD PRIMARY KEY (`id_m_r`),
  ADD KEY `id_mesa_fk` (`id_mesa_fk`),
  ADD KEY `id_region_fk` (`id_region_fk`);

--
-- Indices de la tabla `palabraclaversu`
--
ALTER TABLE `palabraclaversu`
  ADD PRIMARY KEY (`id_palabraclave`);

--
-- Indices de la tabla `problematicaidentificadas`
--
ALTER TABLE `problematicaidentificadas`
  ADD PRIMARY KEY (`id_identificada`);

--
-- Indices de la tabla `problematicaprioritaria`
--
ALTER TABLE `problematicaprioritaria`
  ADD PRIMARY KEY (`id_prioritaria`),
  ADD KEY `id_tema` (`id_tema`);

--
-- Indices de la tabla `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `rsu_c_buscador`
--
ALTER TABLE `rsu_c_buscador`
  ADD PRIMARY KEY (`id_rsu_buscador`),
  ADD KEY `id_eje` (`id_eje`),
  ADD KEY `id_region` (`id_region`),
  ADD KEY `id_pro_gen` (`id_pro_gen`),
  ADD KEY `id_problema` (`id_pro`),
  ADD KEY `id_t` (`id_t`),
  ADD KEY `id_m` (`id_m`);

--
-- Indices de la tabla `rsu_c_eje`
--
ALTER TABLE `rsu_c_eje`
  ADD PRIMARY KEY (`id_eje`);

--
-- Indices de la tabla `rsu_c_mesa`
--
ALTER TABLE `rsu_c_mesa`
  ADD PRIMARY KEY (`id_mesa`);

--
-- Indices de la tabla `rsu_c_palabraclaversu`
--
ALTER TABLE `rsu_c_palabraclaversu`
  ADD PRIMARY KEY (`id_palabraclave`);

--
-- Indices de la tabla `rsu_c_problema`
--
ALTER TABLE `rsu_c_problema`
  ADD PRIMARY KEY (`id_problema`);

--
-- Indices de la tabla `rsu_c_problematica_identificadas`
--
ALTER TABLE `rsu_c_problematica_identificadas`
  ADD PRIMARY KEY (`id_identificada`);

--
-- Indices de la tabla `rsu_c_problematica_prioritaria`
--
ALTER TABLE `rsu_c_problematica_prioritaria`
  ADD PRIMARY KEY (`id_prioritaria`),
  ADD KEY `id_tema` (`id_tema`);

--
-- Indices de la tabla `rsu_c_problema_general`
--
ALTER TABLE `rsu_c_problema_general`
  ADD PRIMARY KEY (`id_p_ge_rsu`);

--
-- Indices de la tabla `rsu_c_region`
--
ALTER TABLE `rsu_c_region`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `rsu_c_temas`
--
ALTER TABLE `rsu_c_temas`
  ADD PRIMARY KEY (`id_temas`),
  ADD KEY `id_eje_fk` (`id_eje_fk`);

--
-- Indices de la tabla `temas`
--
ALTER TABLE `temas`
  ADD PRIMARY KEY (`id_temas`),
  ADD KEY `id_eje` (`id_eje`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asistentes`
--
ALTER TABLE `asistentes`
  MODIFY `id_asistente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `eje`
--
ALTER TABLE `eje`
  MODIFY `id_eje` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `estrategiaoportunidades`
--
ALTER TABLE `estrategiaoportunidades`
  MODIFY `id_estrategia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mesa`
--
ALTER TABLE `mesa`
  MODIFY `id_mesa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `mesa_por_region`
--
ALTER TABLE `mesa_por_region`
  MODIFY `id_m_r` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `palabraclaversu`
--
ALTER TABLE `palabraclaversu`
  MODIFY `id_palabraclave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `problematicaidentificadas`
--
ALTER TABLE `problematicaidentificadas`
  MODIFY `id_identificada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `problematicaprioritaria`
--
ALTER TABLE `problematicaprioritaria`
  MODIFY `id_prioritaria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `region`
--
ALTER TABLE `region`
  MODIFY `id_region` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `rsu_c_buscador`
--
ALTER TABLE `rsu_c_buscador`
  MODIFY `id_rsu_buscador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT de la tabla `rsu_c_eje`
--
ALTER TABLE `rsu_c_eje`
  MODIFY `id_eje` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `rsu_c_mesa`
--
ALTER TABLE `rsu_c_mesa`
  MODIFY `id_mesa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rsu_c_palabraclaversu`
--
ALTER TABLE `rsu_c_palabraclaversu`
  MODIFY `id_palabraclave` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rsu_c_problema`
--
ALTER TABLE `rsu_c_problema`
  MODIFY `id_problema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT de la tabla `rsu_c_problematica_identificadas`
--
ALTER TABLE `rsu_c_problematica_identificadas`
  MODIFY `id_identificada` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rsu_c_problematica_prioritaria`
--
ALTER TABLE `rsu_c_problematica_prioritaria`
  MODIFY `id_prioritaria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rsu_c_problema_general`
--
ALTER TABLE `rsu_c_problema_general`
  MODIFY `id_p_ge_rsu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `rsu_c_region`
--
ALTER TABLE `rsu_c_region`
  MODIFY `id_region` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `rsu_c_temas`
--
ALTER TABLE `rsu_c_temas`
  MODIFY `id_temas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `temas`
--
ALTER TABLE `temas`
  MODIFY `id_temas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asistentes`
--
ALTER TABLE `asistentes`
  ADD CONSTRAINT `id_region` FOREIGN KEY (`id_region`) REFERENCES `rsu_c_region` (`id_region`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mesa_por_region`
--
ALTER TABLE `mesa_por_region`
  ADD CONSTRAINT `mesa_por_region_ibfk_1` FOREIGN KEY (`id_mesa_fk`) REFERENCES `rsu_c_mesa` (`id_mesa`),
  ADD CONSTRAINT `mesa_por_region_ibfk_2` FOREIGN KEY (`id_region_fk`) REFERENCES `rsu_c_region` (`id_region`);

--
-- Filtros para la tabla `rsu_c_buscador`
--
ALTER TABLE `rsu_c_buscador`
  ADD CONSTRAINT `rsu_c_buscador_ibfk_1` FOREIGN KEY (`id_eje`) REFERENCES `rsu_c_eje` (`id_eje`),
  ADD CONSTRAINT `rsu_c_buscador_ibfk_2` FOREIGN KEY (`id_region`) REFERENCES `rsu_c_region` (`id_region`),
  ADD CONSTRAINT `rsu_c_buscador_ibfk_3` FOREIGN KEY (`id_pro_gen`) REFERENCES `rsu_c_problema_general` (`id_p_ge_rsu`),
  ADD CONSTRAINT `rsu_c_buscador_ibfk_4` FOREIGN KEY (`id_pro`) REFERENCES `rsu_c_problema` (`id_problema`),
  ADD CONSTRAINT `rsu_c_buscador_ibfk_5` FOREIGN KEY (`id_t`) REFERENCES `rsu_c_temas` (`id_temas`),
  ADD CONSTRAINT `rsu_c_buscador_ibfk_6` FOREIGN KEY (`id_m`) REFERENCES `rsu_c_mesa` (`id_mesa`);

--
-- Filtros para la tabla `rsu_c_problematica_prioritaria`
--
ALTER TABLE `rsu_c_problematica_prioritaria`
  ADD CONSTRAINT `id_tema` FOREIGN KEY (`id_tema`) REFERENCES `rsu_c_temas` (`id_temas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rsu_c_temas`
--
ALTER TABLE `rsu_c_temas`
  ADD CONSTRAINT `rsu_c_temas_ibfk_1` FOREIGN KEY (`id_eje_fk`) REFERENCES `rsu_c_eje` (`id_eje`);

--
-- Filtros para la tabla `temas`
--
ALTER TABLE `temas`
  ADD CONSTRAINT `id_eje` FOREIGN KEY (`id_eje`) REFERENCES `eje` (`id_eje`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
