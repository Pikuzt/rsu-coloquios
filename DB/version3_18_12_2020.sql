/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 5.7.24 : Database - cuo-rsu-coloquios
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cuo-rsu-coloquios` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cuo-rsu-coloquios`;

/*Table structure for table `asistentes` */

DROP TABLE IF EXISTS `asistentes`;

CREATE TABLE `asistentes` (
  `id_asistente` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Asistente` varchar(150) NOT NULL,
  `Grado_Academico` varchar(150) NOT NULL,
  `Cargo` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `Area_Academica` varchar(150) NOT NULL,
  PRIMARY KEY (`id_asistente`),
  KEY `id_region` (`id_region`),
  CONSTRAINT `id_region` FOREIGN KEY (`id_region`) REFERENCES `rsu_c_region` (`id_region`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `asistentes` */

/*Table structure for table `estrategiaoportunidades` */

DROP TABLE IF EXISTS `estrategiaoportunidades`;

CREATE TABLE `estrategiaoportunidades` (
  `id_estrategia` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `Nombre_Oportunidad` varchar(150) NOT NULL,
  PRIMARY KEY (`id_estrategia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `estrategiaoportunidades` */

/*Table structure for table `mesa_por_region` */

DROP TABLE IF EXISTS `mesa_por_region`;

CREATE TABLE `mesa_por_region` (
  `id_m_r` int(11) NOT NULL AUTO_INCREMENT,
  `id_mesa_fk` int(11) DEFAULT NULL,
  `id_region_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_m_r`),
  KEY `id_mesa_fk` (`id_mesa_fk`),
  KEY `id_region_fk` (`id_region_fk`),
  CONSTRAINT `mesa_por_region_ibfk_1` FOREIGN KEY (`id_mesa_fk`) REFERENCES `rsu_c_mesa` (`id_mesa`),
  CONSTRAINT `mesa_por_region_ibfk_2` FOREIGN KEY (`id_region_fk`) REFERENCES `rsu_c_region` (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mesa_por_region` */

insert  into `mesa_por_region`(`id_m_r`,`id_mesa_fk`,`id_region_fk`) values 
(1,1,1);

/*Table structure for table `rsu_c_buscador` */

DROP TABLE IF EXISTS `rsu_c_buscador`;

CREATE TABLE `rsu_c_buscador` (
  `id_rsu_buscador` int(11) NOT NULL AUTO_INCREMENT,
  `id_eje` int(11) DEFAULT NULL,
  `id_region` int(11) DEFAULT NULL,
  `id_pro_gen` int(11) DEFAULT NULL,
  `id_pro` int(11) DEFAULT NULL,
  `id_t` int(11) DEFAULT NULL,
  `id_m` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rsu_buscador`),
  KEY `id_eje` (`id_eje`),
  KEY `id_region` (`id_region`),
  KEY `id_pro_gen` (`id_pro_gen`),
  KEY `id_problema` (`id_pro`),
  KEY `id_t` (`id_t`),
  KEY `id_m` (`id_m`),
  CONSTRAINT `rsu_c_buscador_ibfk_1` FOREIGN KEY (`id_eje`) REFERENCES `rsu_c_eje` (`id_eje`),
  CONSTRAINT `rsu_c_buscador_ibfk_2` FOREIGN KEY (`id_region`) REFERENCES `rsu_c_region` (`id_region`),
  CONSTRAINT `rsu_c_buscador_ibfk_3` FOREIGN KEY (`id_pro_gen`) REFERENCES `rsu_c_problema_general` (`id_p_ge_rsu`),
  CONSTRAINT `rsu_c_buscador_ibfk_4` FOREIGN KEY (`id_pro`) REFERENCES `rsu_c_problema` (`id_problema`),
  CONSTRAINT `rsu_c_buscador_ibfk_5` FOREIGN KEY (`id_t`) REFERENCES `rsu_c_temas` (`id_temas`),
  CONSTRAINT `rsu_c_buscador_ibfk_6` FOREIGN KEY (`id_m`) REFERENCES `rsu_c_mesa` (`id_mesa`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `rsu_c_buscador` */

insert  into `rsu_c_buscador`(`id_rsu_buscador`,`id_eje`,`id_region`,`id_pro_gen`,`id_pro`,`id_t`,`id_m`) values 
(1,1,3,20,1,3,1),
(2,1,2,6,5,25,1),
(3,1,3,9,6,5,2),
(4,1,2,4,7,2,2),
(5,2,2,5,13,1,3),
(6,7,3,9,14,6,3),
(7,8,4,4,5,20,2),
(8,4,4,4,22,18,2),
(9,7,3,3,25,18,1),
(10,2,1,6,4,15,2),
(11,6,1,1,9,12,2);

/*Table structure for table `rsu_c_eje` */

DROP TABLE IF EXISTS `rsu_c_eje`;

CREATE TABLE `rsu_c_eje` (
  `id_eje` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_eje` varchar(150) NOT NULL,
  PRIMARY KEY (`id_eje`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_eje` */

insert  into `rsu_c_eje`(`id_eje`,`Nombre_eje`) values 
(1,'Salud'),
(2,'Educación'),
(3,'Violencias estructurales'),
(4,'Desigualdades sociales'),
(5,'Sustentabilidad'),
(6,'Mercado, producción, consumo e innovación tecnológica'),
(7,'Construcción democrática'),
(8,'Todos'),
(9,'Democracia');

/*Table structure for table `rsu_c_mesa` */

DROP TABLE IF EXISTS `rsu_c_mesa`;

CREATE TABLE `rsu_c_mesa` (
  `id_mesa` int(10) NOT NULL AUTO_INCREMENT,
  `NombreMesa` varchar(150) NOT NULL,
  PRIMARY KEY (`id_mesa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_mesa` */

insert  into `rsu_c_mesa`(`id_mesa`,`NombreMesa`) values 
(1,'1'),
(2,'2'),
(3,'3');

/*Table structure for table `rsu_c_palabraclaversu` */

DROP TABLE IF EXISTS `rsu_c_palabraclaversu`;

CREATE TABLE `rsu_c_palabraclaversu` (
  `id_palabraclave` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre-PalabraClave` varchar(150) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_oportunidad` int(11) NOT NULL,
  `id_prioritaria` int(11) NOT NULL,
  `id_identificada` int(11) NOT NULL,
  PRIMARY KEY (`id_palabraclave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_palabraclaversu` */

/*Table structure for table `rsu_c_problema` */

DROP TABLE IF EXISTS `rsu_c_problema`;

CREATE TABLE `rsu_c_problema` (
  `id_problema` int(11) NOT NULL AUTO_INCREMENT,
  `problema_p` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_problema`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

/*Data for the table `rsu_c_problema` */

insert  into `rsu_c_problema`(`id_problema`,`problema_p`) values 
(1,'Obesidad'),
(2,'Diabetes\r\n'),
(3,'Enfermedades gastrointestinales\r\n'),
(4,'Lactancia materna (prácticas inadecuadas)\r\n'),
(5,'Enfermedades de transmisión sexual\r\n'),
(6,'Adicciones\r\n'),
(7,'Hipertensión\r\n'),
(8,'Cáncer\r\n'),
(9,'Ansiedad y depresión\r\n'),
(10,'Estrés'),
(11,'Desnutrición'),
(12,'Salud mental\r\n'),
(13,'VIH\r\n'),
(14,'Embarazo infantil, embarazo adolescente, embarazo no deseado'),
(15,'Automediación'),
(16,'No conciencia de la salud y mala alimentación'),
(17,'Apatía y desinterés de la población'),
(18,'Malos hábitos saludables'),
(19,'Dengue'),
(20,'No actividades físicas'),
(21,'Sedentarismo'),
(22,'No hay formación en promoción de la salud y en medicina preventiva, educación en salud'),
(23,'No se busca atención médica integral'),
(24,'Ausencia de plan de desarrollo para la comunidad'),
(25,'Políticas institucionales en salud fragmentadas, sin dirección, poco coherentes'),
(26,'Ausencia de políticas públicas'),
(27,'Desarticulación entre la academia y la gestión administrativa'),
(28,'Acciones aisladas'),
(29,'No hay planeación de carga laboral, exceso de carga laboral'),
(30,'Escasa o falta de voluntad política'),
(31,'No implementación del código de ética'),
(32,'Inseguridad'),
(33,'Marginación'),
(34,'Contaminación'),
(35,'Acoso'),
(36,'Violencia naturalizada'),
(37,'Falta de infraestructura para actividades físicas, entornos no favorables'),
(38,'No organización de horarios que permitan actividad física'),
(39,'Monodisciplina y falta de interculturalidad en salud'),
(40,'No apertura a otras disciplinas'),
(41,'Escaso diálogo con la medicina tradicional'),
(42,'Desconocimiento de la diversidad lingüística (lenguas originarias)'),
(43,'Desconocimiento de usos y costumbres de la comunidad'),
(44,'No comunicación y respeto a la diversidad lingüística'),
(45,'Falta de vinculación'),
(46,'No comunicación hacia el interior ni tampoco al exterior'),
(47,'Falta de intersectorialidad'),
(48,'Ausencia de transversalización en salud'),
(49,'No está en PTE ni el PGD 2030'),
(50,'No hay educación emocional, falta trabajo en actitudes'),
(51,'Crisis de valores: individualismo, egoísmo, enajenación, falta de autonomía'),
(52,'Se forma en la técnica no en la ciudadanía'),
(53,'Segundo idioma, enseñanza de ética'),
(54,'No se incluye en Pladea'),
(55,'Sobrecarga de trabajo para los empleados'),
(56,'Se forma para la inserción laboral, no para la mejora del medio'),
(57,'Perfil de egreso no contempla necesidades sociales'),
(58,'Desarticulación de IES y sector empresarial'),
(59,'Disparidad entre lo que se enseña en la escuela y lo que exigen centros de trabajo'),
(60,'Articulación nula entre instancias de vinculación'),
(61,'Burocratización'),
(62,'Contacto de estudiantes con la realidad se da hasta última parte de su formación'),
(63,'Relación con otros sectores débil'),
(64,'Falta de convenios colaborativos'),
(65,'Insuficiente difusión de temas transversales'),
(66,'Ausencia de objetivos del milenio'),
(67,'Falta de sensibilización a problemas ambientales, de minorías, grupos vulnerables'),
(68,'Erradicar acoso, machismo'),
(69,'Dificultad para relacionar docencia, investigación y extensión '),
(70,'Debilidad institucional en la enseñanza de la investigación'),
(71,'No se conoce lo que hay de investigación y docencia'),
(72,'Trabajo fragmentado'),
(73,'Multitud de comisiones para la realización de proyectos'),
(74,'Trabajo aislado, desarticulado'),
(75,'Ingreso, desarrollo (formación y actualización), Diversificación de carga académica'),
(76,'Falta de motivación en docentes'),
(77,'Expertos en disciplinas sin formación pedagógica'),
(78,'Crisis de valores'),
(79,'Bajos salarios'),
(80,'Sobrecarga de trabajo'),
(81,'Falta de acompañamiento psicológico y emocional'),
(82,'Falta de formación con respecto a ética'),
(83,'Evaluación vocacional'),
(84,'Cobertura insuficiente'),
(85,'Decisiones políticas para nombrar a los directivos'),
(86,'No gobierno'),
(87,'No seguimiento de convenios de colaboración en cuanto cambia administración'),
(88,'Falta de visión de autoridades'),
(89,'Resistencia al cambio'),
(90,'Pasividad'),
(91,'No conciencia de que es necesario un cambio de modelo'),
(92,'Apatía'),
(93,'Desinterés'),
(94,'No se cultiva la lectura'),
(95,'Violencia en la comunidad universitaria'),
(96,'Falta de trabajo en equipo'),
(97,'Se requiere pago de cuotas de estudiantes'),
(98,'Es complicado el transporte'),
(99,'Diseño de aulas no adecuados para evaluación integral'),
(100,'Número elevado de estudiantes por aula'),
(101,'Carencia de infraestructura'),
(102,'Dispersión geográfica limita la oportunidad de formarse integralmente'),
(103,'En diferentes ámbitos: económicos, sociales, físicos, laboral, inferiorización'),
(104,'Violencia por ser diferentes a los demás'),
(105,'Familia perpetúa modelos racistas'),
(106,'Subordinación'),
(107,'Deshumanización'),
(108,'Falta de reconocimiento a los pueblos originarios'),
(109,'Mala distribución de recursos'),
(110,'Exclusión'),
(111,'limitado acceso a la educación, el empleo, la salud, la vivienda, la alimentación, la recreación, desigualdad salarial '),
(112,'Falta de inversión'),
(113,'Creación de empleos'),
(114,'Salarios precarios'),
(115,'Inflación'),
(116,'Legislación universitaria no colabora a combatir violencia de diferentes tipos (de género, por ejemplo), contradictorias'),
(117,'Políticas públicas insuficintes'),
(118,'Falta de empatía'),
(119,'Patriarcado'),
(120,'Falta de sororidad'),
(121,'Aceptación');

/*Table structure for table `rsu_c_problema_general` */

DROP TABLE IF EXISTS `rsu_c_problema_general`;

CREATE TABLE `rsu_c_problema_general` (
  `id_p_ge_rsu` int(11) NOT NULL AUTO_INCREMENT,
  `problema_general` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_p_ge_rsu`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `rsu_c_problema_general` */

insert  into `rsu_c_problema_general`(`id_p_ge_rsu`,`problema_general`) values 
(1,'Carrera académica'),
(2,'Cobertura, ingreso y deserción '),
(3,'Carencias en la vinculación'),
(4,'Ausencia de RSU\r\n'),
(5,'Deficiente cultura y educación en salud'),
(6,'Cultura universitaria '),
(7,'Crisis económica \r\n'),
(8,'Cultura de violencia \r\n'),
(9,'Deficiente sistema institucional para atención de violencia \r\n'),
(10,'Crisis social, económica, ambiental, de valores \r\n'),
(11,'Enfermedades crónico degenerativas '),
(12,'Entornos no favorables, no saludables, no congruentes'),
(13,'Discriminación, racismo, segregación social, marginación socioeconómica\r\n'),
(14,'Desconocimiento de nuestros derechos \r\n'),
(15,'Desigualdad de oportunidades \r\n'),
(16,'Educación. Falta de calidad e interculturalidad \r\n'),
(17,'Entornos no favorables, no saludables, no congruentes\r\n'),
(18,'Exclusión y discriminación \r\n'),
(19,'Estructura legislativa, política y de gobierno  \r\n'),
(20,'Falta de formación \r\n'),
(21,'Falta de promoción de un enfoque sustentable \r\n'),
(22,'Falta de formación y cultura emprendedora \r\n'),
(23,'Falta de infraestructura económica, tecnológica y de innovación '),
(24,'Falta de participación ciudadana \r\n'),
(25,'Formación integral insuficiente \r\n'),
(26,'Formación integral Falta de vinculación \r\n'),
(27,'Insuficiente transversalización de temas contemporáneos \r\n'),
(28,'Falta de sistema de gobernanza y sistema instituciona\r\n'),
(29,'Falta de vinculación \r\n'),
(30,'Inadecuada gestión de recursos \r\n'),
(31,'Infraestructura inadecuada\r\n'),
(32,'Insuficiente trabajo interdisciplinario \r\n'),
(33,'Instituciones débiles \r\n'),
(34,'Falta de vinculación \r\n'),
(35,'Problemas de administración, gobernanza y estructura institucional \r\n'),
(36,'No permea RSU\r\n'),
(37,'Recursos e infraestructura insuficiente \r\n'),
(38,'Normatividad insuficiente, políticas\r\n'),
(39,'No promoción de salud integral\r\n'),
(40,'Violencia \r\n'),
(41,'Pobreza, desempleo, ingresos bajo\r\n'),
(42,'Producción alimentaria y Desnutrición\r\n'),
(43,'No ejercicio de derechos \r\n'),
(44,'Prácticas culturales, compromiso social y ética inadecuadas \r\n'),
(45,'Políticas y normatividad insuficientes\r\n'),
(46,'Lo político en la universidad \r\n'),
(47,'No permea la RS\r\n'),
(48,'Problemas de administración, gobernanza y estructura institucional \r\n');

/*Table structure for table `rsu_c_problematica_identificadas` */

DROP TABLE IF EXISTS `rsu_c_problematica_identificadas`;

CREATE TABLE `rsu_c_problematica_identificadas` (
  `id_identificada` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Problematica` varchar(250) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  PRIMARY KEY (`id_identificada`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_problematica_identificadas` */

/*Table structure for table `rsu_c_problematica_prioritaria` */

DROP TABLE IF EXISTS `rsu_c_problematica_prioritaria`;

CREATE TABLE `rsu_c_problematica_prioritaria` (
  `id_prioritaria` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_ProblePrincipal` varchar(150) NOT NULL,
  `id_mesa` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_eje` int(11) NOT NULL,
  `id_tema` int(11) NOT NULL,
  PRIMARY KEY (`id_prioritaria`),
  KEY `id_tema` (`id_tema`),
  CONSTRAINT `id_tema` FOREIGN KEY (`id_tema`) REFERENCES `rsu_c_temas` (`id_temas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_problematica_prioritaria` */

/*Table structure for table `rsu_c_region` */

DROP TABLE IF EXISTS `rsu_c_region`;

CREATE TABLE `rsu_c_region` (
  `id_region` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_Region` varchar(150) NOT NULL,
  PRIMARY KEY (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_region` */

insert  into `rsu_c_region`(`id_region`,`Nombre_Region`) values 
(1,'Xalapa '),
(2,'Orizaba'),
(3,'Poza Rica'),
(4,'Veracruz ');

/*Table structure for table `rsu_c_temas` */

DROP TABLE IF EXISTS `rsu_c_temas`;

CREATE TABLE `rsu_c_temas` (
  `id_temas` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre_tema` varchar(150) DEFAULT NULL,
  `id_eje_fk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_temas`),
  KEY `id_eje_fk` (`id_eje_fk`),
  CONSTRAINT `rsu_c_temas_ibfk_1` FOREIGN KEY (`id_eje_fk`) REFERENCES `rsu_c_eje` (`id_eje`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf32;

/*Data for the table `rsu_c_temas` */

insert  into `rsu_c_temas`(`id_temas`,`Nombre_tema`,`id_eje_fk`) values 
(1,'Agua, energía, clima',5),
(2,'Ciudades y comunidades sustentables',5),
(3,'Desarrollo de avances tecnológicos y científicos en salud',1),
(4,'Desigualdades educativas',4),
(5,'Desigualdades lingüísticas',4),
(6,'Discriminación y racismo',3),
(7,'Docencia, investigación, extensión',2),
(8,'Ecosistemas terrestres y marinos',5),
(9,'Ejercicio físico y salud en espacios universitarios',5),
(10,'Formación integral del estudiantado/del profesorado',2),
(11,'Exclusión',3),
(12,'Empleo\r\n',6),
(13,'Emprendimiento\r\n',6),
(14,'Gobernanza y gestión educativa institucional',2),
(15,'Homogeneidad\r\n',3),
(16,'Hambre/Soberanía alimentaria\r\n',4),
(17,'Industria e infraestructura',6),
(18,'Inequidad y desigualdad\r\n',3),
(19,'Movilidad humana\r\n',4),
(20,'Innovación',6),
(21,'Interculturalidad',9),
(22,'Instituciones sólidas\r\n',9),
(23,'Participación en el desarrollo de políticas y acciones de salud pública',1),
(24,'Participación comunitaria para la promoción de acciones de salud integral',1),
(25,'Pertinencia e impacto social\r\n',2),
(26,'Pobreza\r\n',4),
(27,'Movimientos sociales\r\n',9),
(28,'Participación comunitaria para la promoción de acciones de salud integral',1),
(29,'Transversalización de temas sociales emergentes en los procesos educativos\r\n',2),
(30,'Subordinación\r\n',3),
(31,'Sistemas socioambientales\r\n',5),
(32,'Sustentabilidad humana',5),
(33,'Sistemas socioambientales\r\n',5);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
