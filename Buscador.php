<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="./css/style.css">
</head>

<div class="container">
       <p>
           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
       </p>

    <div class="container">
        <div class="row">
            <div class="col-sm-6 ">
                <img class="responsive" id="mapa" src="img/mapa-mexico.png">
            </div>
            <div class="col-sm-6 ">
                <div class="contenedorBuscador">
                    <div class="itemBuscador">
                        <p class="name-item">
                            Seleccionar Región
                        </p>
                        <div>
                            <select class="select-item" name="region">
                                <option>Xalapa</option>
                                <option>Veracruz</option>
                            </select>
                        </div>
                    </div>
                    <div class="itemBuscador">
                        <div>
                            <p class="name-item">
                                Seleccionar Eje
                            </p>
                            <select  class="select-item" name="eje">
                                <option>Salud</option>
                                <option>Educación</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>