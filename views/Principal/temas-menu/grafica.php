<?php





?>



<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <!-- Styles -->
    <style>
        #chartdiv {
            width: 100%;
            height: 300px;
        }
    </style>
    <br>


    <div class="d-flex flex-column col-12 bg-success text-white  text-center">
        <h5>Temas con mayor mención</h5>
    </div>
    <!-- HTML -->
    <div id="chartdiv"></div>

</div>

<style>
   tspan{

   }


</style>


<script>






    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_frozen);
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data





        var inf = <?php  echo json_encode($rsu->dataJsonGrafica()); ?>;



        console.log(inf)



        chart.data = <?php  echo $rsu->dataJsonGrafica(); ?>






// Create axes

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 2;
        categoryAxis.renderer.labels.template.disabled = true;
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = "visits";
        series.dataFields.categoryX = "country";
        series.name = "Visits";
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
        series.columns.template.fillOpacity = .8;

        var columnTemplate = series.columns.template;
        columnTemplate.strokeWidth = 2;
        columnTemplate.strokeOpacity = 1;



    }); // end am4core.ready()



</script>
