<?php

require_once dirname(__FILE__).'/../../../Controller/cRsu.php';
$id = $_REQUEST['id'];

//var_dump($id);
$rsu->setIdNube($id);
$texto = $rsu->nube();
foreach ( $texto as $item ){
    $muestra[] = $item->muestra;
}
$textos = json_encode($muestra);









?>

<div id="chartdivnube"></div>



<script>
    am4core.ready(function() {

        let text2 = '<?php echo $textos?>';

        let a = text2.replace('Falta ','');
        
        //console.log(a)

        

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        var chart = am4core.create("chartdivnube", am4plugins_wordCloud.WordCloud);
        var series = chart.series.push(new am4plugins_wordCloud.WordCloudSeries());

        series.accuracy = 4;
        series.step = 15;
        series.rotationThreshold = 0.7;
        series.maxCount = 200;
        series.minWordLength = 2;
        series.labels.template.tooltipText = "{word}: {value}";
        series.fontFamily = "Courier New";
        series.maxFontSize = am4core.percent(30);

        series.text = text2;
    }); // end am4core.ready()
</script>