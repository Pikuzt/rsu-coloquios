
<?php
require_once dirname(__FILE__).'/../../../Controller/cRsu.php';
$rsu->setRegion($_REQUEST['region']);
$rsu->setEje($_REQUEST['eje']);
include_once '../AreaEncabezado/area.php';
?>
<style>
    section{
        background: #ffffff !important;
    }
</style>


<?php if ($rsu->informacionTabla() == 0): ?>
<?php include 'sin_datos.php'?>
<?php else: ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">




                <table id="search">
                    <tr>
                        <td> <strong style="padding: 1rem">Tema:</strong>   </td>
                        <td id="search1"></td>
                    <tr>

                </table>
                <br>

                <div class="table-responsive">
                    <table class="table table-hover table-sm" id="example">
                        <thead class="">
                        <tr>
                            <th scope="col">Región</th>
                            <th scope="col">Tema</th>
                            <th scope="col">Problemática General</th>
                            <th scope="col">problematica</th>


                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rsu->informacionTabla() as $item): ?>
                            <tr>
                                <td><?php echo $item->Nombre_Region ?></td>
                                <td><?php echo $item->Nombre_tema ?></td>
                                <td><?php echo $item->problema_general ?></td>
                                <td><?php echo $item->problema_p ?></td>


                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-3">

                <hr>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 p-3">
                <?php include_once 'temas.php'?>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 p-3">
                <?php include_once 'grafica.php'?>
            </div>
            <script>
                $(document).ready(function () {


                    $('#myModal').modal('hide');
                    $('#example').DataTable({

                        initComplete: function(){
                            this.api().columns().every(function(i){
                                var column = this,
                                    select = $('<select class="form-control""><option value="">Opción</option></select>')
                                        .appendTo( $('#search'+i))
                                        .on( 'change', function(){
                                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                            column.search( val ? '^'+val+'$' : '', true, false ).draw();
                                        });
                                column.data().unique().sort().each( function ( d, j ) {
                                    select.append('<option value="'+d+'">'+d+'</option>');
                                });

                                console.log(select);
                            });


                        },


                        /*
                        searchPanes:{
                            cascadePanes:true,
                            dtOpts:{
                                dom:'tp',
                                paging:'true',
                                pagingType:'simple',
                                searching:false
                            }
                        },
                        dom:'Pfrtip',*/

                        "language":
                            {
                                "sProcessing":    "Procesando...",
                                "sLengthMenu":    "Mostrar _MENU_ registros",
                                "sZeroRecords":   "No se encontraron resultados",
                                "sEmptyTable":    "Ningún dato disponible en esta tabla",
                                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":   "",
                                "sSearch":        "Buscar:",
                                "sUrl":           "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":    "Último",
                                    "sNext":    "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                    });



                });
            </script>
        </div>


    </div>

<?php endif; ?>



