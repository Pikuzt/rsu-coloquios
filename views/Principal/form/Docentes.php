
<style>
    #docente-1 {
        width: 100%;
        height: 500px;
    }

</style>


<div class="container">
    <div class="row table-resposive">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover table-sm" id="example">
                    <thead class="">




                    <tr>
                        <th scope="col text-center" >Región</th>
                        <th scope="col text-center">Salud</th>
                        <th scope="col text-center">Educación</th>
                        <th scope="col text-center">Violencias de estructurales</th>
                        <th scope="col text-center">Desigualdades Sociales</th>
                        <th scope="col text-center">Sustentabilidad</th>
                        <th scope="col text-center">Mercado,Produccion Consumo e Innovacion Tecnologica </th>
                        <th scope="col text-center">Construcción Democratica </th>
                        <th scope="col text-center">Total </th>

                    </tr>
                    </thead>
                    <tbody>

                        <tr class="text-center">
                            <td>Poza Rica – Tuxpan</td>
                            <td>3</td>
                            <td>3</td>
                            <td>4</td>
                            <td>5</td>
                            <td>7</td>
                            <td>3</td>
                            <td>5</td>
                            <td>30</td>
                        </tr>

                        <tr class="text-center">
                            <td>Xalapa</td>
                            <td>13</td>
                            <td>4</td>
                            <td>4</td>
                            <td>1</td>
                            <td>8</td>
                            <td>2</td>
                            <td>0</td>
                            <td>32</td>
                        </tr>

                        <tr class="text-center">
                            <td>Córdoba - Orizaba</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                        </tr>

                        <tr class="text-center">
                            <td>Veracruz</td>
                            <td>10</td>
                            <td>6</td>
                            <td>9</td>
                            <td>8</td>
                            <td>14</td>
                            <td>15</td>
                            <td>4</td>
                            <td>66</td>
                        </tr>

                        <tr class="text-center">
                            <td>Coatzacoalcos – Minatitlán</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>0</td>
                            <td>1</td>
                            <td>0</td>
                            <td>0</td>
                            <td>1</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div id="docente-1"></div>
        </div>
    </div>
</div>



<script>
    am4core.ready(function() {

// Themes begin
// Themes end

// Create chart instance
        var chart = am4core.create("docente-1", am4charts.XYChart);

// Add data
        chart.data = [ {
            "year": "Salud",
            "europe": 1,
            "namerica": 30,
            "asia": 66,
            "lamerica": 32,
            "meast": 0,
            "africa": 0.1
        }, {
            "year": "Educación",
            "europe": 0,
            "namerica": 3,
            "asia": 10,
            "lamerica": 13,
            "meast": 0,
            "africa": 0.1
        }, {
            "year": "VE",
            "europe": 0,
            "namerica": 4,
            "asia": 9,
            "lamerica":4 ,
            "meast": 0,
            "africa": 0.1
        },
            {
                "year": "DS",
                "europe": 0,
                "namerica": 5,
                "asia":8 ,
                "lamerica": 1,
                "meast": 0,
                "africa": 0.1
            },
            {
                "year": "Sustentabilidad",
                "europe": 1,
                "namerica": 7,
                "asia": 14,
                "lamerica": 8,
                "meast": 0,
                "africa": 0.1
            },
            {
                "year": "MPCeIT",
                "europe":0 ,
                "namerica": 3,
                "asia": 15,
                "lamerica": 2,
                "meast": 0,
                "africa": 0.1
            },
            {
                "year": "CD",
                "europe":0,
                "namerica": 5,
                "asia": 4,
                "lamerica": 0,
                "meast": 0,
                "africa": 0.1
            }];

// Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "year";
        categoryAxis.title.text = "Ejes";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;

        var  valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.title.text = "Frecuencias";

// Create series
        function createSeries(field, name, stacked) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "year";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
            series.stacked = stacked;
            series.columns.template.width = am4core.percent(95);
        }

        createSeries("europe", "Coatzacoalcos", true);
        createSeries("namerica", "Poza Rica - Tuxpan", true);
        createSeries("asia", "Veracruz", true);
        createSeries("lamerica", "Xalapa", true);
        createSeries("meast", "Córdoba - Orizaba", true);


// Add legend
        chart.legend = new am4charts.Legend();

    }); // end am4core.ready()
</script>



