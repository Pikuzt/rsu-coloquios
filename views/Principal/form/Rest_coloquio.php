



<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
            <img class="responsive" id="mapa" src="Assets/img/m.png">
        </div>

        <form id="buscar">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                <div class="contenedorBuscador  ">

                    <div id="error"></div>
                    <div class="itemBuscador rsu-re p-2 ">
                        <div class="rsu-rueda"> <p>1</p></div>
                        <p class="name-item item-rsu">
                            Seleccionar Región
                        </p>
                        <div >
                            <select class="select-item" name="region" id="region">
                                <option>Opción</option>
                                <?php foreach ($rsu->regiones() as $itemR): ?>
                                    <option value="<?php echo$itemR->id_region ?>"><?php echo$itemR->Nombre_Region ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="itemBuscador rsu-re p-2">
                        <div>
                            <div class="rsu-rueda"> <p>2</p></div>
                            <p class="name-item item-rsu">
                                Seleccionar Eje
                            </p>
                            <select  class="select-item" name="eje" id="eje">
                                <option>Opción</option>
                                <?php foreach ($rsu->eje() as $itemE): ?>
                                    <option value="<?php echo$itemE->id_eje ?>"><?php echo$itemE->Nombre_eje ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

        </form>

    </div>
</div>

<section id="resultado"></section>
