<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover table-sm" id="example">
                    <thead class="">
                    <tr>
                        <th scope="col">Tema</th>
                        <th scope="col">Problemática General</th>
                        <th scope="col">problematica</th>
                        <th scope="col">Región</th>
                        <th scope="col">Mesa</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($rsu->informacionTabla() as $item): ?>
                        <tr>
                            <td><?php echo $item->Nombre_tema ?></td>
                            <td><?php echo $item->problema_general ?></td>
                            <td><?php echo $item->problema_p ?></td>
                            <td><?php echo $item->Nombre_Region ?></td>
                            <td><?php echo $item->NombreMesa ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>