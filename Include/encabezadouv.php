<!DOCTYPE html>
<html lang="en">
<head>
    <title>Coloquio Responsabilidad Social Universitaria</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-sm">
    <!-- Brand/logo -->
    <a class="navbar-brand" >
        <img src="img/uv-null.png" alt="logo" style="width:40px;" class="d-inline-block align-top" alt="">Universidad Veracruzana
    </a>

    <!-- Links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="#">Proyecto</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Acerca de</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Contacto</a>
        </li>
    </ul>
</nav>


</body>
</html>
